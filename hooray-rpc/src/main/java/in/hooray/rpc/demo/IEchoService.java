package in.hooray.rpc.demo;

import com.baidu.jprotobuf.pbrpc.ProtobufRPC;

/**
 * RPC的方法必须要指定@ProtobufRPC注解. serviceName与methodName要与服务端保持一致。
 * 这里未指定methodName，则使用方法的名称 "echo"
 * @author daqing
 *
 */
public interface IEchoService {

	/**
     * To define a RPC client method. <br>
     * serviceName is "echoService"
     * methodName is use default method name "echo"
     * onceTalkTimeout is 200 milliseconds
     * 
     * @param info
     * @return
     */
    @ProtobufRPC(serviceName = "echoService", onceTalkTimeout = 2000)
    EchoInfo echo(EchoInfo info);
}
