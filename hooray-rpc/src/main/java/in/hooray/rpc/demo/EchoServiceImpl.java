package in.hooray.rpc.demo;

import com.baidu.jprotobuf.pbrpc.ProtobufRPCService;

/**
 * 服务发布的RPC方法必须用@ProtobufPRCService注解进行标识
 * @author Administrator
 *
 */
public class EchoServiceImpl implements IEchoService{

	@ProtobufRPCService(serviceName = "echoService", methodName = "echo")
	@Override
	public EchoInfo echo(EchoInfo info) {
		 EchoInfo ret = new EchoInfo();
	     ret.setMessage("hello:" + info.message);
	     return ret;
	}

}
