package in.hooray.rpc.demo;

import com.baidu.jprotobuf.pbrpc.client.ProtobufRpcProxy;
import com.baidu.jprotobuf.pbrpc.transport.RpcClient;

public class Rpcclient {

	public static void main(String[] args) throws Exception {
		System.out.println(2 + 01);
		RpcClient rpcClient = new RpcClient();
		// 创建EchoService代理
		ProtobufRpcProxy<IEchoService> pbrpcProxy = new ProtobufRpcProxy<IEchoService>(rpcClient, IEchoService.class);
		pbrpcProxy.setPort(1031);
		
		// 动态生成代理实例
		IEchoService echoService = pbrpcProxy.proxy();
		EchoInfo request = new EchoInfo();
		request.message = "hello";
		EchoInfo response = echoService.echo(request);
		System.out.println(response.message);
		rpcClient.stop();
		
	}

}
