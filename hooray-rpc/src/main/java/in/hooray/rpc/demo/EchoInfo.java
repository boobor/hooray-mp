package in.hooray.rpc.demo;

import com.baidu.bjf.remoting.protobuf.annotation.Protobuf;

public class EchoInfo {
	
	/**
	 * 注解方式的定义可以极大简化大家的工作量，下面等同于的IDEL配置
	 * <pre>
	 * 	package pkg;  
	 * 	option java_package = "com.baidu.bjf.remoting.protobuf.rpc";
	 * 	//这里声明输出的java的类名  
	 * 	option java_outer_classname = "EchoInfo";  
	 * 	message InterClassName {  
	 *	  required string message = 1;
	 *	} 
	 * </pre>
	 */
	@Protobuf
    public String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
