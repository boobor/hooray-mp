package in.hooray.mpsdk.plugin.impl;

import org.springframework.stereotype.Component;

import in.hooray.mpsdk.plugin.IPlugin;
import in.hooray.mpsdk.plugin.PluginInfo;
import in.hooray.mpsdk.plugin.PluginParam;

/**
 * 首次订阅关注
 * @author darking
 *
 */
@Component
public class SubscribeResponsePlugin implements IPlugin {

	@Override
	public Object execute(PluginParam param) {
		
		return null;
	}

	@Override
	public PluginInfo getPluginInfo(String pluginName) {
		return null;
	}

}
