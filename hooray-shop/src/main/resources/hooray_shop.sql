-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        10.1.8-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 hooray.shop_account_log 结构
DROP TABLE IF EXISTS `shop_account_log`;
CREATE TABLE IF NOT EXISTS `shop_account_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL COMMENT '管理员ID',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0增加,1减少',
  `event` tinyint(3) NOT NULL COMMENT '操作类型，意义请看accountLog类',
  `time` datetime NOT NULL COMMENT '发生时间',
  `amount` decimal(15,2) NOT NULL COMMENT '金额',
  `amount_log` decimal(15,2) NOT NULL COMMENT '每次增减后面的金额记录',
  `note` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `shop_account_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户余额日志表';

-- 正在导出表  hooray.shop_account_log 的数据：~0 rows (大约)
DELETE FROM `shop_account_log`;
/*!40000 ALTER TABLE `shop_account_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_account_log` ENABLE KEYS */;


-- 导出  表 hooray.shop_address 结构
DROP TABLE IF EXISTS `shop_address`;
CREATE TABLE IF NOT EXISTS `shop_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `accept_name` varchar(20) NOT NULL COMMENT '收货人姓名',
  `zip` varchar(6) DEFAULT NULL COMMENT '邮编',
  `telphone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `country` int(11) unsigned DEFAULT NULL COMMENT '国ID',
  `province` int(11) unsigned NOT NULL COMMENT '省ID',
  `city` int(11) unsigned NOT NULL COMMENT '市ID',
  `area` int(11) unsigned NOT NULL COMMENT '区ID',
  `address` varchar(250) NOT NULL COMMENT '收货地址',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否默认,0:为非默认,1:默认',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收货信息表';

-- 正在导出表  hooray.shop_address 的数据：~0 rows (大约)
DELETE FROM `shop_address`;
/*!40000 ALTER TABLE `shop_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_address` ENABLE KEYS */;


-- 导出  表 hooray.shop_attribute 结构
DROP TABLE IF EXISTS `shop_attribute`;
CREATE TABLE IF NOT EXISTS `shop_attribute` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性ID',
  `model_id` int(11) unsigned DEFAULT NULL COMMENT '模型ID',
  `type` tinyint(1) DEFAULT NULL COMMENT '输入控件的类型,1:单选,2:复选,3:下拉,4:输入框',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `value` text COMMENT '属性值(逗号分隔)',
  `search` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否支持搜索0不支持1支持',
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`,`search`),
  CONSTRAINT `shop_attribute_ibfk_1` FOREIGN KEY (`model_id`) REFERENCES `shop_model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='属性表';

-- 正在导出表  hooray.shop_attribute 的数据：~0 rows (大约)
DELETE FROM `shop_attribute`;
/*!40000 ALTER TABLE `shop_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_attribute` ENABLE KEYS */;


-- 导出  表 hooray.shop_bill 结构
DROP TABLE IF EXISTS `shop_bill`;
CREATE TABLE IF NOT EXISTS `shop_bill` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) unsigned NOT NULL COMMENT '商家ID',
  `apply_time` datetime DEFAULT NULL COMMENT '申请结算时间',
  `pay_time` datetime DEFAULT NULL COMMENT '支付结算时间',
  `admin_id` int(11) unsigned DEFAULT NULL COMMENT '管理员ID',
  `is_pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未结算,1:已结算',
  `apply_content` text COMMENT '申请结算文本',
  `pay_content` text COMMENT '支付结算文本',
  `start_time` date DEFAULT NULL COMMENT '结算起始时间',
  `end_time` date DEFAULT NULL COMMENT '结算终止时间',
  `log` text COMMENT '结算明细',
  `order_goods_ids` text COMMENT 'order_goods表主键ID，结算的ID',
  PRIMARY KEY (`id`),
  KEY `seller_id` (`seller_id`),
  CONSTRAINT `shop_bill_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `shop_seller` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家货款结算单表';

-- 正在导出表  hooray.shop_bill 的数据：~0 rows (大约)
DELETE FROM `shop_bill`;
/*!40000 ALTER TABLE `shop_bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_bill` ENABLE KEYS */;


-- 导出  表 hooray.shop_brand 结构
DROP TABLE IF EXISTS `shop_brand`;
CREATE TABLE IF NOT EXISTS `shop_brand` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '品牌ID',
  `name` varchar(255) NOT NULL COMMENT '品牌名称',
  `logo` varchar(255) DEFAULT NULL COMMENT 'logo地址',
  `url` varchar(255) DEFAULT NULL COMMENT '网址',
  `description` text COMMENT '描述',
  `sort` smallint(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `category_ids` varchar(255) DEFAULT NULL COMMENT '品牌分类,逗号分割id',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`),
  KEY `category_ids` (`category_ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='品牌表';

-- 正在导出表  hooray.shop_brand 的数据：~0 rows (大约)
DELETE FROM `shop_brand`;
/*!40000 ALTER TABLE `shop_brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_brand` ENABLE KEYS */;


-- 导出  表 hooray.shop_brand_category 结构
DROP TABLE IF EXISTS `shop_brand_category`;
CREATE TABLE IF NOT EXISTS `shop_brand_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `goods_category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品分类ID',
  PRIMARY KEY (`id`),
  KEY `goods_category_id` (`goods_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='品牌分类表';

-- 正在导出表  hooray.shop_brand_category 的数据：~0 rows (大约)
DELETE FROM `shop_brand_category`;
/*!40000 ALTER TABLE `shop_brand_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_brand_category` ENABLE KEYS */;


-- 导出  表 hooray.shop_category 结构
DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE IF NOT EXISTS `shop_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `parent_id` int(11) unsigned NOT NULL COMMENT '父分类ID',
  `sort` smallint(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `visibility` tinyint(1) NOT NULL DEFAULT '1' COMMENT '首页是否显示 1显示 0 不显示',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'SEO关键词和检索关键词',
  `descript` varchar(255) DEFAULT NULL COMMENT 'SEO描述',
  `title` varchar(255) DEFAULT NULL COMMENT 'SEO标题title',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`,`visibility`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品分类表';

-- 正在导出表  hooray.shop_category 的数据：~0 rows (大约)
DELETE FROM `shop_category`;
/*!40000 ALTER TABLE `shop_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_category` ENABLE KEYS */;


-- 导出  表 hooray.shop_category_extend 结构
DROP TABLE IF EXISTS `shop_category_extend`;
CREATE TABLE IF NOT EXISTS `shop_category_extend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  `category_id` int(11) unsigned NOT NULL COMMENT '商品分类ID',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `shop_category_extend_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_category_extend_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `shop_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品扩展分类表';

-- 正在导出表  hooray.shop_category_extend 的数据：~0 rows (大约)
DELETE FROM `shop_category_extend`;
/*!40000 ALTER TABLE `shop_category_extend` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_category_extend` ENABLE KEYS */;


-- 导出  表 hooray.shop_collection_doc 结构
DROP TABLE IF EXISTS `shop_collection_doc`;
CREATE TABLE IF NOT EXISTS `shop_collection_doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL COMMENT '订单号',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `time` datetime NOT NULL COMMENT '时间',
  `payment_id` int(11) unsigned NOT NULL COMMENT '支付方式ID',
  `admin_id` int(11) unsigned DEFAULT NULL COMMENT '管理员id',
  `pay_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付状态，0:准备，1:支付成功',
  `note` text COMMENT '收款备注',
  `if_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未删除 1:删除',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `if_del` (`if_del`),
  KEY `payment_id` (`payment_id`),
  CONSTRAINT `shop_collection_doc_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收款单';

-- 正在导出表  hooray.shop_collection_doc 的数据：~0 rows (大约)
DELETE FROM `shop_collection_doc`;
/*!40000 ALTER TABLE `shop_collection_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_collection_doc` ENABLE KEYS */;


-- 导出  表 hooray.shop_commend_goods 结构
DROP TABLE IF EXISTS `shop_commend_goods`;
CREATE TABLE IF NOT EXISTS `shop_commend_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commend_id` int(11) unsigned NOT NULL COMMENT '推荐类型ID 1:最新商品 2:特价商品 3:热卖排行 4:推荐商品',
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `commend_id` (`commend_id`),
  CONSTRAINT `shop_commend_goods_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推荐类商品';

-- 正在导出表  hooray.shop_commend_goods 的数据：~0 rows (大约)
DELETE FROM `shop_commend_goods`;
/*!40000 ALTER TABLE `shop_commend_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_commend_goods` ENABLE KEYS */;


-- 导出  表 hooray.shop_delivery 结构
DROP TABLE IF EXISTS `shop_delivery`;
CREATE TABLE IF NOT EXISTS `shop_delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '快递名称',
  `description` varchar(50) DEFAULT NULL COMMENT '快递描述',
  `area_groupid` text COMMENT '配送区域id',
  `firstprice` text COMMENT '配送地址对应的首重价格',
  `secondprice` text COMMENT '配送地区对应的续重价格',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '配送类型 0先付款后发货 1先发货后付款 2自提点',
  `first_weight` int(11) unsigned NOT NULL COMMENT '首重重量(克)',
  `second_weight` int(11) unsigned NOT NULL COMMENT '续重重量(克)',
  `first_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '首重价格',
  `second_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '续重价格',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开启状态',
  `sort` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  `is_save_price` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否支持物流保价 1支持保价 0  不支持保价',
  `save_rate` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '保价费率',
  `low_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '最低保价',
  `price_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '费用类型 0统一设置 1指定地区费用',
  `open_default` tinyint(1) NOT NULL DEFAULT '1' COMMENT '启用默认费用 1启用 0 不启用',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0:未删除 1:删除',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='配送方式表';

-- 正在导出表  hooray.shop_delivery 的数据：~2 rows (大约)
DELETE FROM `shop_delivery`;
/*!40000 ALTER TABLE `shop_delivery` DISABLE KEYS */;
INSERT INTO `shop_delivery` (`id`, `name`, `description`, `area_groupid`, `firstprice`, `secondprice`, `type`, `first_weight`, `second_weight`, `first_price`, `second_price`, `status`, `sort`, `is_save_price`, `save_rate`, `low_price`, `price_type`, `open_default`, `is_delete`) VALUES
	(1, '快递', '直接由第三方物流公司配送', 'N;', 'N;', 'N;', 0, 1000, 1000, 20.00, 20.00, 1, 0, 0, 0.00, 0.00, 0, 0, 0),
	(2, '货到付款', '直接由第三方物流公司配送', 'N;', 'N;', 'N;', 1, 1000, 1000, 20.00, 20.00, 1, 0, 0, 0.00, 0.00, 0, 0, 0);
/*!40000 ALTER TABLE `shop_delivery` ENABLE KEYS */;


-- 导出  表 hooray.shop_delivery_doc 结构
DROP TABLE IF EXISTS `shop_delivery_doc`;
CREATE TABLE IF NOT EXISTS `shop_delivery_doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '发货单ID',
  `order_id` int(11) unsigned NOT NULL COMMENT '订单ID',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `admin_id` int(11) unsigned DEFAULT '0' COMMENT '管理员ID',
  `seller_id` int(11) unsigned DEFAULT '0' COMMENT '商户ID',
  `name` varchar(255) NOT NULL COMMENT '收货人',
  `postcode` varchar(6) DEFAULT NULL COMMENT '邮编',
  `telphone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `country` int(11) unsigned DEFAULT NULL COMMENT '国ID',
  `province` int(11) unsigned NOT NULL COMMENT '省ID',
  `city` int(11) unsigned NOT NULL COMMENT '市ID',
  `area` int(11) unsigned NOT NULL COMMENT '区ID',
  `address` varchar(250) NOT NULL COMMENT '收货地址',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `time` datetime NOT NULL COMMENT '创建时间',
  `freight` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `delivery_code` varchar(255) NOT NULL COMMENT '物流单号',
  `delivery_type` varchar(255) NOT NULL COMMENT '物流方式',
  `note` text COMMENT '管理员添加的备注信息',
  `if_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未删除 1:已删除',
  `freight_id` int(11) unsigned NOT NULL COMMENT '货运公司ID',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `freight_id` (`freight_id`),
  CONSTRAINT `shop_delivery_doc_ibfk_1` FOREIGN KEY (`freight_id`) REFERENCES `shop_freight_company` (`id`),
  CONSTRAINT `shop_delivery_doc_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发货单';

-- 正在导出表  hooray.shop_delivery_doc 的数据：~0 rows (大约)
DELETE FROM `shop_delivery_doc`;
/*!40000 ALTER TABLE `shop_delivery_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_delivery_doc` ENABLE KEYS */;


-- 导出  表 hooray.shop_expresswaybill 结构
DROP TABLE IF EXISTS `shop_expresswaybill`;
CREATE TABLE IF NOT EXISTS `shop_expresswaybill` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL COMMENT '快递单模板名字',
  `config` text COMMENT '序列化的快递单结构数据',
  `background` varchar(255) DEFAULT NULL COMMENT '背景图片路径',
  `width` smallint(5) unsigned DEFAULT '900' COMMENT '背景图片路径',
  `height` smallint(5) unsigned DEFAULT '600' COMMENT '背景图片路径',
  `is_close` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 1关闭,0正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='快递单模板';

-- 正在导出表  hooray.shop_expresswaybill 的数据：~5 rows (大约)
DELETE FROM `shop_expresswaybill`;
/*!40000 ALTER TABLE `shop_expresswaybill` DISABLE KEYS */;
INSERT INTO `shop_expresswaybill` (`id`, `name`, `config`, `background`, `width`, `height`, `is_close`) VALUES
	(1, 'EMS特快专递', 'a:12:{i:1;s:222:"{"x":461,"y":350,"typeId":"date_y","typeText":"当前日期-年","width":33,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":18}";i:5;s:227:"{"x":70,"y":184,"typeId":"dly_address","typeText":"发货人-地址","width":318,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":54}";i:6;s:224:"{"x":271,"y":117,"typeId":"dly_tel","typeText":"发货人-电话","width":119,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":20}";i:10;s:225:"{"x":642,"y":237,"typeId":"ship_zip","typeText":"收货人-邮编","width":109,"styleSheet":{"trackingLeft":"9","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":19}";i:11;s:228:"{"x":625,"y":117,"typeId":"ship_mobile","typeText":"收货人-手机","width":126,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":21}";i:0;s:222:"{"x":510,"y":348,"typeId":"date_m","typeText":"当前日期-月","width":29,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":21}";i:9;s:224:"{"x":458,"y":370,"typeId":"order_memo","typeText":"订单-备注","width":292,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":30}";i:4;s:224:"{"x":122,"y":120,"typeId":"dly_name","typeText":"发货人-姓名","width":94,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":24}";i:8;s:223:"{"x":301,"y":238,"typeId":"dly_zip","typeText":"发货人-邮编","width":99,"styleSheet":{"trackingLeft":"9","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":19}";i:3;s:246:"{"x":409,"y":184,"typeId":"ship_detail_addr","typeText":"收货人-地区+详细地址","width":343,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":55}";i:2;s:226:"{"x":462,"y":117,"typeId":"ship_name","typeText":"收货人-姓名","width":104,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":27}";i:7;s:222:"{"x":559,"y":348,"typeId":"date_d","typeText":"当前日期-日","width":30,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":21}";}', 'upload/2011/11/30/20111130103004185.jpg', 900, 500, 0),
	(2, '顺丰速运', 'a:12:{i:1;s:224:"{"x":265,"y":104,"typeId":"dly_name","typeText":"发货人-姓名","width":74,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":22}";i:3;s:222:"{"x":549,"y":314,"typeId":"date_m","typeText":"当前日期-月","width":32,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":22}";i:0;s:224:"{"x":522,"y":426,"typeId":"order_memo","typeText":"订单-备注","width":182,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":35}";i:6;s:224:"{"x":54,"y":337,"typeId":"ship_tel","typeText":"收货人-电话","width":143,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":17}";i:11;s:227:"{"x":207,"y":188,"typeId":"dly_mobile","typeText":"发货人-手机","width":129,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":18}";i:4;s:245:"{"x":55,"y":271,"typeId":"ship_detail_addr","typeText":"收货人-地区+详细地址","width":284,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":56}";i:5;s:228:"{"x":376,"y":118,"typeId":"ship_time","typeText":"订单-送货时间","width":52,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":22}";i:10;s:228:"{"x":207,"y":336,"typeId":"ship_mobile","typeText":"收货人-手机","width":132,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":18}";i:7;s:227:"{"x":82,"y":132,"typeId":"dly_address","typeText":"发货人-地址","width":255,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":52}";i:8;s:225:"{"x":267,"y":219,"typeId":"ship_name","typeText":"收货人-姓名","width":72,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":23}";i:2;s:223:"{"x":51,"y":190,"typeId":"dly_tel","typeText":"发货人-电话","width":152,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":18}";i:9;s:222:"{"x":585,"y":314,"typeId":"date_d","typeText":"当前日期-日","width":33,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":20}";}', 'upload/2011/11/30/20111130110521690.jpg', 900, 550, 0),
	(3, '申通快递', 'a:9:{i:1;s:222:"{"typeId":"date_d","x":153,"typeText":"当前日期-日","y":389,"width":22,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":20}";i:6;s:221:"{"typeId":"date_y","x":67,"typeText":"当前日期-年","y":391,"width":35,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":22}";i:8;s:224:"{"typeId":"dly_name","x":121,"typeText":"发货人-姓名","y":97,"width":114,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":28}";i:3;s:228:"{"typeId":"dly_address","x":114,"typeText":"发货人-地址","y":167,"width":296,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":75}";i:2;s:227:"{"typeId":"dly_mobile","x":140,"typeText":"发货人-手机","y":240,"width":270,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":24}";i:4;s:222:"{"typeId":"date_m","x":113,"typeText":"当前日期-月","y":391,"width":24,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":18}";i:5;s:228:"{"typeId":"ship_mobile","x":508,"typeText":"收货人-手机","y":241,"width":274,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":21}";i:0;s:225:"{"typeId":"ship_name","x":487,"typeText":"收货人-姓名","y":95,"width":115,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":29}";i:7;s:246:"{"typeId":"ship_detail_addr","x":474,"typeText":"收货人-地区+详细地址","y":167,"width":310,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":72}";}', 'upload/2011/11/30/20111130111842435.jpg', 900, 520, 0),
	(4, '宅急送', 'a:13:{i:2;s:225:"{"typeId":"ship_tel","x":429,"typeText":"收货人-电话","y":234,"width":139,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":24}";i:8;s:226:"{"typeId":"ship_name","x":456,"typeText":"收货人-姓名","y":120,"width":123,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":31}";i:1;s:227:"{"typeId":"dly_mobile","x":254,"typeText":"发货人-手机","y":237,"width":130,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":25}";i:11;s:225:"{"typeId":"dly_name","x":114,"typeText":"发货人-姓名","y":123,"width":120,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":28}";i:10;s:222:"{"typeId":"date_d","x":120,"typeText":"当前日期-日","y":438,"width":23,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":16}";i:6;s:223:"{"typeId":"dly_tel","x":90,"typeText":"发货人-电话","y":235,"width":132,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":26}";i:9;s:231:"{"typeId":"dly_area_1","x":255,"typeText":"发货人-地区2级","y":123,"width":127,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":27}";i:12;s:246:"{"typeId":"ship_detail_addr","x":428,"typeText":"收货人-地区+详细地址","y":151,"width":294,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":57}";i:3;s:231:"{"typeId":"dly_area_1","x":599,"typeText":"发货人-地区2级","y":122,"width":123,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":29}";i:5;s:221:"{"typeId":"date_m","x":90,"typeText":"当前日期-月","y":438,"width":27,"styleSheet":{"fontWeight":"normal","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","trackingLeft":"0"},"height":17}";i:4;s:224:"{"typeId":"order_memo","x":445,"typeText":"订单-备注","y":364,"width":166,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":32}";i:7;s:228:"{"typeId":"ship_mobile","x":585,"typeText":"收货人-手机","y":234,"width":136,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":22}";i:0;s:227:"{"typeId":"dly_address","x":86,"typeText":"发货人-地址","y":154,"width":286,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":55}";}', 'upload/2011/11/30/20111130112722985.jpg', 900, 520, 0),
	(5, '中通速递', 'a:11:{i:2;s:224:"{"typeId":"order_memo","x":372,"typeText":"订单-备注","y":369,"width":145,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":34}";i:10;s:222:"{"typeId":"date_d","x":159,"typeText":"当前日期-日","y":379,"width":24,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":17}";i:7;s:228:"{"typeId":"dly_address","x":130,"typeText":"发货人-地址","y":137,"width":259,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":59}";i:4;s:222:"{"typeId":"date_m","x":118,"typeText":"当前日期-月","y":379,"width":34,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":19}";i:0;s:225:"{"typeId":"dly_name","x":129,"typeText":"发货人-姓名","y":103,"width":101,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":25}";i:8;s:226:"{"typeId":"ship_name","x":476,"typeText":"收货人-姓名","y":103,"width":110,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":22}";i:6;s:224:"{"typeId":"dly_tel","x":122,"typeText":"发货人-电话","y":242,"width":125,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":27}";i:9;s:225:"{"typeId":"ship_zip","x":642,"typeText":"收货人-邮编","y":244,"width":106,"styleSheet":{"trackingLeft":"9","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":23}";i:1;s:246:"{"typeId":"ship_detail_addr","x":476,"typeText":"收货人-地区+详细地址","y":134,"width":262,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":61}";i:5;s:224:"{"typeId":"dly_zip","x":291,"typeText":"发货人-邮编","y":243,"width":103,"styleSheet":{"trackingLeft":"9","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":27}";i:3;s:228:"{"typeId":"ship_mobile","x":464,"typeText":"收货人-手机","y":241,"width":142,"styleSheet":{"trackingLeft":"0","fontSize":"12","fontStyle":"normal","fontFamily":"宋体","textAlign":"left","fontWeight":"normal"},"height":30}";}', 'upload/2011/11/30/20111130113535527.jpg', 900, 520, 0);
/*!40000 ALTER TABLE `shop_expresswaybill` ENABLE KEYS */;


-- 导出  表 hooray.shop_freight_company 结构
DROP TABLE IF EXISTS `shop_freight_company`;
CREATE TABLE IF NOT EXISTS `shop_freight_company` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `freight_type` varchar(255) NOT NULL COMMENT '货运类型',
  `freight_name` varchar(255) NOT NULL COMMENT '货运公司名称',
  `url` varchar(255) NOT NULL COMMENT '网址',
  `sort` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未删除1删除',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='货运公司';

-- 正在导出表  hooray.shop_freight_company 的数据：~16 rows (大约)
DELETE FROM `shop_freight_company`;
/*!40000 ALTER TABLE `shop_freight_company` DISABLE KEYS */;
INSERT INTO `shop_freight_company` (`id`, `freight_type`, `freight_name`, `url`, `sort`, `is_del`) VALUES
	(1, 'EMS', '邮政特快专递', 'http://www.ems.com.cn', 0, 0),
	(2, 'STO', '申通快递', 'http://www.sto.cn', 0, 0),
	(3, 'HHTT', '天天快递', 'http://www.ttkd.cn', 0, 0),
	(4, 'YTO', '圆通速递', 'http://www.yto.net.cn', 0, 0),
	(5, 'SF', '顺丰速运', 'http://www.sf-express.com', 0, 0),
	(6, 'YD', '韵达快递', 'http://www.yundaex.com', 0, 0),
	(7, 'ZTO', '中通速递', 'http://www.zto.cn', 0, 0),
	(8, 'LB', '龙邦物流', 'http://www.lbex.com.cn', 0, 0),
	(9, 'ZJS', '宅急送', 'http://www.zjs.com.cn', 0, 0),
	(10, 'UAPEX', '全一快递', 'http://www.apex100.com', 0, 0),
	(11, 'HTKY', '汇通速递', 'http://www.htky365.com', 0, 0),
	(12, 'MHKD', '民航快递', 'http://www.cae.com.cn', 0, 0),
	(13, 'ZTKY', '中铁快运', 'http://www.cre.cn', 0, 0),
	(14, 'FEDEX', 'FedEx联邦快递', 'http://www.fedex.com/cn', 0, 0),
	(15, 'DHL', 'DHL', 'http://www.cn.dhl.com', 0, 0),
	(16, 'DBL', '德邦物流', 'http://www.deppon.com', 0, 0);
/*!40000 ALTER TABLE `shop_freight_company` ENABLE KEYS */;


-- 导出  表 hooray.shop_goods 结构
DROP TABLE IF EXISTS `shop_goods`;
CREATE TABLE IF NOT EXISTS `shop_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `name` varchar(50) NOT NULL COMMENT '商品名称',
  `goods_no` varchar(20) NOT NULL COMMENT '商品的货号',
  `model_id` int(11) unsigned NOT NULL COMMENT '模型ID',
  `sell_price` decimal(15,2) NOT NULL COMMENT '销售价格',
  `market_price` decimal(15,2) DEFAULT NULL COMMENT '市场价格',
  `cost_price` decimal(15,2) DEFAULT NULL COMMENT '成本价格',
  `up_time` datetime DEFAULT NULL COMMENT '上架时间',
  `down_time` datetime DEFAULT NULL COMMENT '下架时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `store_nums` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `img` varchar(255) DEFAULT NULL COMMENT '原图',
  `ad_img` varchar(255) DEFAULT NULL COMMENT '宣传图',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除 0正常 1已删除 2下架 3申请上架',
  `content` text COMMENT '商品描述',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'SEO关键词',
  `description` varchar(255) DEFAULT NULL COMMENT 'SEO描述',
  `search_words` varchar(50) DEFAULT NULL COMMENT '产品搜索词库,逗号分隔',
  `weight` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '重量',
  `point` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `unit` varchar(10) DEFAULT NULL COMMENT '计量单位',
  `brand_id` int(11) DEFAULT NULL COMMENT '品牌ID',
  `visit` int(11) NOT NULL DEFAULT '0' COMMENT '浏览次数',
  `favorite` int(11) NOT NULL DEFAULT '0' COMMENT '收藏次数',
  `sort` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  `spec_array` text COMMENT '序列化存储规格,key值为规则ID，value为此商品具有的规格值',
  `exp` int(11) NOT NULL DEFAULT '0' COMMENT '经验值',
  `comments` int(11) NOT NULL DEFAULT '0' COMMENT '评论次数',
  `sale` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `grade` int(11) NOT NULL DEFAULT '0' COMMENT '评分总数',
  `seller_id` int(11) DEFAULT '0' COMMENT '卖家ID',
  `is_share` tinyint(1) NOT NULL DEFAULT '0' COMMENT '共享商品 0不共享 1共享',
  PRIMARY KEY (`id`),
  KEY `seller_id` (`seller_id`),
  KEY `sort` (`sort`),
  KEY `brand_id` (`brand_id`),
  KEY `is_del` (`is_del`),
  KEY `model_id` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品信息表';

-- 正在导出表  hooray.shop_goods 的数据：~0 rows (大约)
DELETE FROM `shop_goods`;
/*!40000 ALTER TABLE `shop_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_goods` ENABLE KEYS */;


-- 导出  表 hooray.shop_goods_attribute 结构
DROP TABLE IF EXISTS `shop_goods_attribute`;
CREATE TABLE IF NOT EXISTS `shop_goods_attribute` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  `attribute_id` int(11) unsigned DEFAULT NULL COMMENT '属性ID',
  `attribute_value` varchar(255) DEFAULT NULL COMMENT '属性值',
  `model_id` int(11) unsigned DEFAULT NULL COMMENT '模型ID',
  `order` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `attribute_id` (`attribute_id`,`attribute_value`),
  KEY `order` (`order`),
  CONSTRAINT `shop_goods_attribute_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='属性值表';

-- 正在导出表  hooray.shop_goods_attribute 的数据：~0 rows (大约)
DELETE FROM `shop_goods_attribute`;
/*!40000 ALTER TABLE `shop_goods_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_goods_attribute` ENABLE KEYS */;


-- 导出  表 hooray.shop_goods_car 结构
DROP TABLE IF EXISTS `shop_goods_car`;
CREATE TABLE IF NOT EXISTS `shop_goods_car` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `content` text COMMENT '购物内容',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_goods_car_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车';

-- 正在导出表  hooray.shop_goods_car 的数据：~0 rows (大约)
DELETE FROM `shop_goods_car`;
/*!40000 ALTER TABLE `shop_goods_car` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_goods_car` ENABLE KEYS */;


-- 导出  表 hooray.shop_goods_photo 结构
DROP TABLE IF EXISTS `shop_goods_photo`;
CREATE TABLE IF NOT EXISTS `shop_goods_photo` (
  `id` char(32) NOT NULL COMMENT '图片的md5值',
  `img` varchar(255) DEFAULT NULL COMMENT '原始图片路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片表';

-- 正在导出表  hooray.shop_goods_photo 的数据：~0 rows (大约)
DELETE FROM `shop_goods_photo`;
/*!40000 ALTER TABLE `shop_goods_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_goods_photo` ENABLE KEYS */;


-- 导出  表 hooray.shop_goods_photo_relation 结构
DROP TABLE IF EXISTS `shop_goods_photo_relation`;
CREATE TABLE IF NOT EXISTS `shop_goods_photo_relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  `photo_id` char(32) NOT NULL DEFAULT '' COMMENT '图片ID,图片的md5值',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `photo_id` (`photo_id`),
  CONSTRAINT `shop_goods_photo_relation_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='相册商品关系表';

-- 正在导出表  hooray.shop_goods_photo_relation 的数据：~0 rows (大约)
DELETE FROM `shop_goods_photo_relation`;
/*!40000 ALTER TABLE `shop_goods_photo_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_goods_photo_relation` ENABLE KEYS */;


-- 导出  表 hooray.shop_group_price 结构
DROP TABLE IF EXISTS `shop_group_price`;
CREATE TABLE IF NOT EXISTS `shop_group_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL COMMENT '产品ID',
  `product_id` int(11) unsigned DEFAULT NULL COMMENT '货品ID',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组ID',
  `price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `group_id` (`group_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `shop_group_price_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_group_price_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `iwebshop_user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录某件商品对于某组会员的价格关系表，优先权大于组设定的折扣率';

-- 正在导出表  hooray.shop_group_price 的数据：~0 rows (大约)
DELETE FROM `shop_group_price`;
/*!40000 ALTER TABLE `shop_group_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_group_price` ENABLE KEYS */;


-- 导出  表 hooray.shop_member 结构
DROP TABLE IF EXISTS `shop_member`;
CREATE TABLE IF NOT EXISTS `shop_member` (
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `true_name` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `telephone` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `area` varchar(255) DEFAULT NULL COMMENT '地区',
  `contact_addr` varchar(250) DEFAULT NULL COMMENT '联系地址',
  `qq` varchar(15) DEFAULT NULL COMMENT 'QQ',
  `msn` varchar(250) DEFAULT NULL COMMENT 'MSN',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '性别1男2女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `group_id` int(11) DEFAULT NULL COMMENT '分组',
  `exp` int(11) NOT NULL DEFAULT '0' COMMENT '经验值',
  `point` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `message_ids` text COMMENT '消息ID',
  `time` datetime NOT NULL COMMENT '注册日期时间',
  `zip` varchar(10) DEFAULT NULL COMMENT '邮政编码',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态 1正常状态 2 删除至回收站 3锁定',
  `prop` text COMMENT '用户拥有的工具',
  `balance` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '用户余额',
  `last_login` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `custom` varchar(255) DEFAULT NULL COMMENT '用户习惯方式,配送和支付方式等信息',
  PRIMARY KEY (`user_id`),
  KEY `group_id` (`group_id`),
  KEY `status` (`status`),
  CONSTRAINT `shop_member_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- 正在导出表  hooray.shop_member 的数据：~0 rows (大约)
DELETE FROM `shop_member`;
/*!40000 ALTER TABLE `shop_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_member` ENABLE KEYS */;


-- 导出  表 hooray.shop_merch_ship_info 结构
DROP TABLE IF EXISTS `shop_merch_ship_info`;
CREATE TABLE IF NOT EXISTS `shop_merch_ship_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ship_name` varchar(255) NOT NULL COMMENT '发货点名称',
  `ship_user_name` varchar(255) NOT NULL COMMENT '发货人姓名',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别 0:女 1:男',
  `country` int(11) DEFAULT NULL COMMENT '国id',
  `province` int(11) NOT NULL COMMENT '省id',
  `city` int(11) NOT NULL COMMENT '市id',
  `area` int(11) NOT NULL COMMENT '地区id',
  `postcode` varchar(6) DEFAULT NULL COMMENT '邮编',
  `address` varchar(255) NOT NULL COMMENT '具体地址',
  `mobile` varchar(20) NOT NULL COMMENT '手机',
  `telphone` varchar(20) DEFAULT NULL COMMENT '电话',
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1为默认地址，0则不是',
  `note` text COMMENT '备注',
  `addtime` datetime NOT NULL COMMENT '保存时间',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为删除，1为显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家发货点信息';

-- 正在导出表  hooray.shop_merch_ship_info 的数据：~0 rows (大约)
DELETE FROM `shop_merch_ship_info`;
/*!40000 ALTER TABLE `shop_merch_ship_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_merch_ship_info` ENABLE KEYS */;


-- 导出  表 hooray.shop_model 结构
DROP TABLE IF EXISTS `shop_model`;
CREATE TABLE IF NOT EXISTS `shop_model` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` varchar(50) NOT NULL COMMENT '模型名称',
  `spec_ids` text COMMENT '规格ID逗号分隔',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模型表';

-- 正在导出表  hooray.shop_model 的数据：~0 rows (大约)
DELETE FROM `shop_model`;
/*!40000 ALTER TABLE `shop_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_model` ENABLE KEYS */;


-- 导出  表 hooray.shop_notify_registry 结构
DROP TABLE IF EXISTS `shop_notify_registry`;
CREATE TABLE IF NOT EXISTS `shop_notify_registry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `email` varchar(255) DEFAULT NULL COMMENT 'emaill',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `register_time` datetime NOT NULL COMMENT '登记时间',
  `notify_time` datetime DEFAULT NULL COMMENT '通知时间',
  `notify_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未通知1已通知',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_notify_registry_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_notify_registry_ibfk_2` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='到货通知表';

-- 正在导出表  hooray.shop_notify_registry 的数据：~0 rows (大约)
DELETE FROM `shop_notify_registry`;
/*!40000 ALTER TABLE `shop_notify_registry` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_notify_registry` ENABLE KEYS */;


-- 导出  表 hooray.shop_online_recharge 结构
DROP TABLE IF EXISTS `shop_online_recharge`;
CREATE TABLE IF NOT EXISTS `shop_online_recharge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `recharge_no` varchar(20) NOT NULL COMMENT '充值单号',
  `account` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `time` datetime NOT NULL COMMENT '时间',
  `payment_name` varchar(80) NOT NULL COMMENT '充值方式名称',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_online_recharge_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线充值表';

-- 正在导出表  hooray.shop_online_recharge 的数据：~0 rows (大约)
DELETE FROM `shop_online_recharge`;
/*!40000 ALTER TABLE `shop_online_recharge` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_online_recharge` ENABLE KEYS */;


-- 导出  表 hooray.shop_order 结构
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE IF NOT EXISTS `shop_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(20) NOT NULL COMMENT '订单号',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `pay_type` int(11) NOT NULL COMMENT '用户支付方式ID,当为0时表示货到付款',
  `distribution` int(11) DEFAULT NULL COMMENT '用户选择的配送ID',
  `status` tinyint(1) DEFAULT '1' COMMENT '订单状态 1生成订单,2支付订单,3取消订单(客户触发),4作废订单(管理员触发),5完成订单,6退款,7部分退款',
  `pay_status` tinyint(1) DEFAULT '0' COMMENT '支付状态 0：未支付; 1：已支付;',
  `distribution_status` tinyint(1) DEFAULT '0' COMMENT '配送状态 0：未发送,1：已发送,2：部分发送',
  `accept_name` varchar(20) NOT NULL COMMENT '收货人姓名',
  `postcode` varchar(6) DEFAULT NULL COMMENT '邮编',
  `telphone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `country` int(11) DEFAULT NULL COMMENT '国ID',
  `province` int(11) DEFAULT NULL COMMENT '省ID',
  `city` int(11) DEFAULT NULL COMMENT '市ID',
  `area` int(11) DEFAULT NULL COMMENT '区ID',
  `address` varchar(250) DEFAULT NULL COMMENT '收货地址',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `payable_amount` decimal(15,2) DEFAULT '0.00' COMMENT '应付商品总金额',
  `real_amount` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '实付商品总金额',
  `payable_freight` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '总运费金额',
  `real_freight` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '实付运费',
  `pay_time` datetime DEFAULT NULL COMMENT '付款时间',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `create_time` datetime DEFAULT NULL COMMENT '下单时间',
  `completion_time` datetime DEFAULT NULL COMMENT '订单完成时间',
  `invoice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '发票：0不索要1索要',
  `postscript` varchar(255) DEFAULT NULL COMMENT '用户附言',
  `note` text COMMENT '管理员备注',
  `if_del` tinyint(1) DEFAULT '0' COMMENT '是否删除1为删除',
  `insured` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '保价',
  `if_insured` tinyint(1) DEFAULT '0' COMMENT '是否保价0:不保价，1保价',
  `pay_fee` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '支付手续费',
  `invoice_title` varchar(100) DEFAULT NULL COMMENT '发票抬头',
  `taxes` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '税金',
  `promotions` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '促销优惠金额',
  `discount` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '订单折扣或涨价',
  `order_amount` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '订单总金额',
  `prop` varchar(255) DEFAULT NULL COMMENT '使用的道具id',
  `accept_time` varchar(80) DEFAULT NULL COMMENT '用户收货时间',
  `exp` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '增加的经验',
  `point` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '增加的积分',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0普通订单,1团购订单,2限时抢购',
  `trade_no` varchar(255) DEFAULT NULL COMMENT '支付平台交易号',
  `takeself` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '自提点ID',
  `checkcode` varchar(255) DEFAULT NULL COMMENT '自提方式的验证码',
  `active_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '促销活动ID',
  PRIMARY KEY (`id`),
  KEY `if_del` (`if_del`),
  KEY `order_no` (`order_no`),
  KEY `user_id` (`user_id`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

-- 正在导出表  hooray.shop_order 的数据：~0 rows (大约)
DELETE FROM `shop_order`;
/*!40000 ALTER TABLE `shop_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_order` ENABLE KEYS */;


-- 导出  表 hooray.shop_order_goods 结构
DROP TABLE IF EXISTS `shop_order_goods`;
CREATE TABLE IF NOT EXISTS `shop_order_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL COMMENT '订单ID',
  `goods_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  `img` varchar(255) NOT NULL COMMENT '商品图片',
  `product_id` int(11) unsigned DEFAULT '0' COMMENT '货品ID',
  `goods_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `real_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '实付金额',
  `goods_nums` int(11) NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goods_weight` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '重量',
  `goods_array` text COMMENT '商品和货品名称name和规格value串json数据格式',
  `is_send` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已发货 0:未发货;1:已发货;2:已经退货',
  `is_checkout` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否给商家结算货款 0:未结算;1:已结算',
  `delivery_id` int(11) NOT NULL DEFAULT '0' COMMENT '配送单ID',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `shop_order_goods_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单商品表';

-- 正在导出表  hooray.shop_order_goods 的数据：~0 rows (大约)
DELETE FROM `shop_order_goods`;
/*!40000 ALTER TABLE `shop_order_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_order_goods` ENABLE KEYS */;


-- 导出  表 hooray.shop_order_log 结构
DROP TABLE IF EXISTS `shop_order_log`;
CREATE TABLE IF NOT EXISTS `shop_order_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned DEFAULT NULL COMMENT '订单id',
  `user` varchar(20) DEFAULT NULL COMMENT '操作人：顾客或admin或seller',
  `action` varchar(20) DEFAULT NULL COMMENT '动作',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  `result` varchar(10) DEFAULT NULL COMMENT '操作的结果',
  `note` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `shop_order_log_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单日志表';

-- 正在导出表  hooray.shop_order_log 的数据：~0 rows (大约)
DELETE FROM `shop_order_log`;
/*!40000 ALTER TABLE `shop_order_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_order_log` ENABLE KEYS */;


-- 导出  表 hooray.shop_payment 结构
DROP TABLE IF EXISTS `shop_payment`;
CREATE TABLE IF NOT EXISTS `shop_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '支付名称',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:线上、2:线下',
  `class_name` varchar(50) NOT NULL COMMENT '支付类名称',
  `description` text COMMENT '描述',
  `logo` varchar(255) NOT NULL COMMENT '支付方式logo图片路径',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '安装状态 0启用 1禁用',
  `order` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  `note` text COMMENT '支付说明',
  `poundage` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `poundage_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '手续费方式 1百分比 2固定值',
  `config_param` text COMMENT '配置参数,json数据对象',
  `client_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:PC端 2:移动端 3:通用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='支付方式表';

-- 正在导出表  hooray.shop_payment 的数据：~16 rows (大约)
DELETE FROM `shop_payment`;
/*!40000 ALTER TABLE `shop_payment` DISABLE KEYS */;
INSERT INTO `shop_payment` (`id`, `name`, `type`, `class_name`, `description`, `logo`, `status`, `order`, `note`, `poundage`, `poundage_type`, `config_param`, `client_type`) VALUES
	(0, '货到付款', 2, 'freight_collect', '货到付款', '/payments/logos/pay_freight_collect.jpg', 1, 99, NULL, 0.00, 1, NULL, 3),
	(1, '预存款', 1, 'balance', '预存款是客户在您网站上的虚拟资金帐户。', '/payments/logos/pay_deposit.gif', 0, 99, NULL, 0.00, 1, NULL, 3),
	(2, '网银在线', 1, 'chinabank', '网银在线是中国领先的电子支付解决方案提供商之一。 <a href="http://www.chinabank.com.cn/" target="_blank">立即申请</a>', '/payments/logos/pay_chinabank.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(3, '银联在线支付', 1, 'unionpay', '银联unionpay平台接口。<a href="https://open.unionpay.com/ajweb/index" target="_blank">立即申请</a>', '/payments/logos/pay_unionpay.png', 1, 99, NULL, 0.00, 1, NULL, 1),
	(4, '银联支付', 1, 'chinapay', '银联chinapay平台接口。<a href="http://www.chinapay.com/" target="_blank">立即申请</a>', '/payments/logos/pay_chinapay.png', 1, 99, NULL, 0.00, 1, NULL, 1),
	(5, '腾讯财付通', 1, 'tenpay', '财付通是腾讯公司创办的中国领先的在线支付平台，致力于为互联网用户和企业提供安全、便捷、专业的在线支付服务。 <a href="https://www.tenpay.com/v2/" target="_blank">立即申请</a>', '/payments/logos/pay_tenpay.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(6, '快钱', 1, 'bill99', '快钱是国内领先的独立第三方支付企业，旨在为各类企业及个人提供安全、便捷和保密的支付清算与账务服务。 <a href="https://www.99bill.com/" target="_blank">立即申请</a>', '/payments/logos/pay_99bill.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(7, '支付宝担保交易', 1, 'trade_alipay', '担保交易支付方式，买家先将交易资金存入支付宝并通知卖家发货，买家确认收货后资金自动进入卖家支付宝账户，完成交易。 <a href="http://www.alipay.com/" target="_blank">立即申请</a>', '/payments/logos/pay_alipay.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(8, '支付宝即时到帐', 1, 'direct_alipay', '即时到帐支付方式，买家的交易资金直接打入卖家支付宝账户，快速回笼交易资金。 <a href="http://www.alipay.com/" target="_blank">立即申请</a>', '/payments/logos/pay_alipay.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(9, '支付宝双接口', 1, 'alipay', '担保+即使双接口支付方式，给于买家充足的自主选择权。 <a href="http://www.alipay.com/" target="_blank">立即申请</a>', '/payments/logos/pay_alipay.gif', 0, 99, NULL, 0.00, 1, NULL, 1),
	(10, '支付宝网银直连', 1, 'bank_alipay', '直接调用银行网关接口，通过网银直接付款，买家无需支付宝账号 <a href="http://www.alipay.com/" target="_blank">立即申请</a>', '/payments/logos/pay_alipay_bank.png', 0, 99, NULL, 0.00, 1, NULL, 1),
	(11, '贝宝', 1, 'paypal', '是全球最大的在线支付平台，同时也是目前全球贸易网上支付标准。<a href="https://www.paypal-biz.com/" target="_blank">立即申请</a>', '/payments/logos/pay_paypal.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(12, '微信移动支付', 1, 'wap_wechat', '微信商城专用支付接口。必须在微信客户端中使用<a href="https://mp.weixin.qq.com/" target="_blank">立即申请</a>', '/payments/logos/pay_wap_wechat.png', 1, 99, NULL, 0.00, 1, NULL, 2),
	(13, '微信二维码支付', 1, 'scan_wechat', '微信二维码支付接口。<a href="https://mp.weixin.qq.com/" target="_blank">立即申请</a>', '/payments/logos/pay_scan_wechat.gif', 1, 99, NULL, 0.00, 1, NULL, 1),
	(14, '线下支付', 2, 'offline', '线下付款结算', '/payments/logos/pay_offline.gif', 1, 99, NULL, 0.00, 0, NULL, 3),
	(15, '支付宝手机网站支付', 1, 'wap_alipay', '手机支付方便用户手机快速下单。 <a href="http://www.alipay.com/" target="_blank">立即申请</a>', '/payments/logos/pay_wap_alipay.png', 0, 99, NULL, 0.00, 1, NULL, 2);
/*!40000 ALTER TABLE `shop_payment` ENABLE KEYS */;


-- 导出  表 hooray.shop_point_log 结构
DROP TABLE IF EXISTS `shop_point_log`;
CREATE TABLE IF NOT EXISTS `shop_point_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `datetime` datetime NOT NULL COMMENT '发生时间',
  `value` int(11) NOT NULL COMMENT '积分增减 增加正数 减少负数',
  `intro` varchar(50) NOT NULL COMMENT '积分改动说明',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_point_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分增减记录表';

-- 正在导出表  hooray.shop_point_log 的数据：~0 rows (大约)
DELETE FROM `shop_point_log`;
/*!40000 ALTER TABLE `shop_point_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_point_log` ENABLE KEYS */;


-- 导出  表 hooray.shop_products 结构
DROP TABLE IF EXISTS `shop_products`;
CREATE TABLE IF NOT EXISTS `shop_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL COMMENT '货品ID',
  `products_no` varchar(20) NOT NULL COMMENT '货品的货号(以商品的货号加横线加数字组成)',
  `spec_array` text COMMENT 'json规格数据',
  `store_nums` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `market_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '市场价格',
  `sell_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '销售价格',
  `cost_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '成本价格',
  `weight` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '重量',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  CONSTRAINT `shop_products_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='货品表';

-- 正在导出表  hooray.shop_products 的数据：~0 rows (大约)
DELETE FROM `shop_products`;
/*!40000 ALTER TABLE `shop_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_products` ENABLE KEYS */;


-- 导出  表 hooray.shop_promotion 结构
DROP TABLE IF EXISTS `shop_promotion`;
CREATE TABLE IF NOT EXISTS `shop_promotion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `condition` int(11) NOT NULL COMMENT '生效条件 type=0时为消费额度 type=1时为goods_id',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '活动类型 0:购物车促销规则 1:商品限时抢购',
  `award_value` varchar(255) DEFAULT NULL COMMENT '奖励值 type=0时奖励值 type=1时为抢购价格',
  `name` varchar(20) NOT NULL COMMENT '活动名称',
  `intro` text COMMENT '活动介绍',
  `award_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '奖励方式:0限时抢购 1减金额 2奖励折扣 3赠送积分 4赠送代金券 5赠送赠品 6免运费',
  `is_close` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭 0:否 1:是',
  `user_group` text COMMENT '允许参与活动的用户组,all表示所有用户组',
  PRIMARY KEY (`id`),
  KEY `condition` (`condition`),
  KEY `start_time` (`start_time`,`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录促销活动的表';

-- 正在导出表  hooray.shop_promotion 的数据：~0 rows (大约)
DELETE FROM `shop_promotion`;
/*!40000 ALTER TABLE `shop_promotion` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_promotion` ENABLE KEYS */;


-- 导出  表 hooray.shop_prop 结构
DROP TABLE IF EXISTS `shop_prop`;
CREATE TABLE IF NOT EXISTS `shop_prop` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '道具名称',
  `card_name` varchar(32) DEFAULT NULL COMMENT '道具的卡号',
  `card_pwd` varchar(32) DEFAULT NULL COMMENT '道具的密码',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `value` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '面值',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '道具类型 0:代金券',
  `condition` varchar(255) DEFAULT NULL COMMENT '条件数据 type=0时,表示ticket的表id,模型id',
  `is_close` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭 0:正常,1:关闭,2:下订单未支付时临时锁定',
  `img` varchar(255) DEFAULT NULL COMMENT '道具图片',
  `is_userd` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否被使用过 0:未使用,1:已使用',
  `is_send` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否被发送过 0:否 1:是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='道具表';

-- 正在导出表  hooray.shop_prop 的数据：~0 rows (大约)
DELETE FROM `shop_prop`;
/*!40000 ALTER TABLE `shop_prop` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_prop` ENABLE KEYS */;


-- 导出  表 hooray.shop_refundment_doc 结构
DROP TABLE IF EXISTS `shop_refundment_doc`;
CREATE TABLE IF NOT EXISTS `shop_refundment_doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `order_id` int(11) unsigned NOT NULL COMMENT '订单id',
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '退款金额',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `admin_id` int(11) unsigned DEFAULT NULL COMMENT '管理员id',
  `pay_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '退款状态，0:申请退款 1:退款失败 2:退款成功',
  `content` text COMMENT '申请退款原因',
  `dispose_time` datetime DEFAULT NULL COMMENT '处理时间',
  `dispose_idea` text COMMENT '处理意见',
  `if_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未删除 1:删除',
  `goods_id` int(11) unsigned NOT NULL COMMENT '要退款的商品',
  `product_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '要退款的货品',
  `seller_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商家ID',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `if_del` (`if_del`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_refundment_doc_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款单';

-- 正在导出表  hooray.shop_refundment_doc 的数据：~0 rows (大约)
DELETE FROM `shop_refundment_doc`;
/*!40000 ALTER TABLE `shop_refundment_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_refundment_doc` ENABLE KEYS */;


-- 导出  表 hooray.shop_regiment 结构
DROP TABLE IF EXISTS `shop_regiment`;
CREATE TABLE IF NOT EXISTS `shop_regiment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '团购标题',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `store_nums` int(11) NOT NULL DEFAULT '0' COMMENT '库存量',
  `sum_count` int(11) NOT NULL DEFAULT '0' COMMENT '已销售量',
  `limit_min_count` int(11) NOT NULL DEFAULT '0' COMMENT '每人限制最少购买数量',
  `limit_max_count` int(11) NOT NULL DEFAULT '0' COMMENT '每人限制最多购买数量',
  `intro` varchar(255) DEFAULT NULL COMMENT '介绍',
  `is_close` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭',
  `regiment_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '团购价格',
  `sell_price` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '原来价格',
  `goods_id` int(11) unsigned NOT NULL COMMENT '关联商品id',
  `img` varchar(255) DEFAULT NULL COMMENT '商品图片',
  `sort` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `is_close` (`is_close`,`start_time`,`end_time`),
  KEY `goods_id` (`goods_id`),
  KEY `sort` (`sort`),
  CONSTRAINT `shop_regiment_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团购';

-- 正在导出表  hooray.shop_regiment 的数据：~0 rows (大约)
DELETE FROM `shop_regiment`;
/*!40000 ALTER TABLE `shop_regiment` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_regiment` ENABLE KEYS */;


-- 导出  表 hooray.shop_seller 结构
DROP TABLE IF EXISTS `shop_seller`;
CREATE TABLE IF NOT EXISTS `shop_seller` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `seller_name` varchar(80) NOT NULL COMMENT '商家登录用户名',
  `password` char(32) NOT NULL COMMENT '商家密码',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间',
  `login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `is_vip` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是特级商家',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未删除,1:已删除',
  `is_lock` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未锁定,1:已锁定',
  `true_name` varchar(80) NOT NULL COMMENT '商家真实名称',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(20) NOT NULL COMMENT '手机号码',
  `phone` varchar(20) NOT NULL COMMENT '座机号码',
  `paper_img` varchar(255) DEFAULT NULL COMMENT '执照证件照片',
  `cash` decimal(15,2) DEFAULT NULL COMMENT '保证金',
  `country` int(11) unsigned DEFAULT NULL COMMENT '国ID',
  `province` int(11) unsigned NOT NULL COMMENT '省ID',
  `city` int(11) unsigned NOT NULL COMMENT '市ID',
  `area` int(11) unsigned NOT NULL COMMENT '区ID',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `account` text COMMENT '收款账号信息',
  `server_num` varchar(20) DEFAULT NULL COMMENT '客服号码',
  `home_url` varchar(255) DEFAULT NULL COMMENT '企业URL网站',
  `sort` smallint(5) NOT NULL DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `seller_name` (`seller_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家表';

-- 正在导出表  hooray.shop_seller 的数据：~0 rows (大约)
DELETE FROM `shop_seller`;
/*!40000 ALTER TABLE `shop_seller` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_seller` ENABLE KEYS */;


-- 导出  表 hooray.shop_spec 结构
DROP TABLE IF EXISTS `shop_spec`;
CREATE TABLE IF NOT EXISTS `shop_spec` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '规格名称',
  `value` text COMMENT '规格值',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '显示类型 1文字 2图片',
  `note` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除1删除',
  `seller_id` int(11) DEFAULT '0' COMMENT '商家ID',
  PRIMARY KEY (`id`),
  KEY `is_del` (`is_del`),
  KEY `seller_id` (`seller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规格表';

-- 正在导出表  hooray.shop_spec 的数据：~0 rows (大约)
DELETE FROM `shop_spec`;
/*!40000 ALTER TABLE `shop_spec` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_spec` ENABLE KEYS */;


-- 导出  表 hooray.shop_spec_photo 结构
DROP TABLE IF EXISTS `shop_spec_photo`;
CREATE TABLE IF NOT EXISTS `shop_spec_photo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `name` varchar(100) DEFAULT NULL COMMENT '图片名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规格图片表';

-- 正在导出表  hooray.shop_spec_photo 的数据：~0 rows (大约)
DELETE FROM `shop_spec_photo`;
/*!40000 ALTER TABLE `shop_spec_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_spec_photo` ENABLE KEYS */;


-- 导出  表 hooray.shop_withdraw 结构
DROP TABLE IF EXISTS `shop_withdraw`;
CREATE TABLE IF NOT EXISTS `shop_withdraw` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `time` datetime NOT NULL COMMENT '时间',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `name` varchar(255) NOT NULL COMMENT '开户姓名',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '-1失败,0未处理,1处理中,2成功',
  `note` varchar(255) DEFAULT NULL COMMENT '用户备注',
  `re_note` varchar(255) DEFAULT NULL COMMENT '回复备注信息',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未删除,1已删除',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shop_withdraw_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `iwebshop_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现记录';

-- 正在导出表  hooray.shop_withdraw 的数据：~0 rows (大约)
DELETE FROM `shop_withdraw`;
/*!40000 ALTER TABLE `shop_withdraw` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_withdraw` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
