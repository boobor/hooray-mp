package in.hooray.shop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration   
@ImportResource(locations={"mybatise-spring.xml"})
@ComponentScan(basePackages={"in.hooray.shop", "in.hooray.entity", "in.hooray.service"})
public class GlobalDbConfiguration {

}
