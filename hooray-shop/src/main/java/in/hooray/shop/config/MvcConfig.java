package in.hooray.shop.config;

import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

import in.hooray.core.web.servlet.CaptchaServlet;


@Configuration
// @EnableWebMvc  // 启动了该配置，无法直接访问类路径下的静态目录`public`，将会由SpringMVC的DispatchServlet进行分发
@ComponentScan(basePackages={"in.hooray"})
public class MvcConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
	@Qualifier(value="beetlConfig")
	private BeetlGroupUtilConfiguration configuration;
	
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		
        configurer.enable();
    }
	
	@Bean(name="beetlConfig")
	public BeetlGroupUtilConfiguration beetlGroupUtilConfiguration() {
		BeetlGroupUtilConfiguration b = new BeetlGroupUtilConfiguration();
		ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
		b.setResourceLoader(resourceLoader);
		b.init();
		return b;
	}
	
	@Bean(name="viewResolver")
	public BeetlSpringViewResolver beetlSpringViewResolver() {
		BeetlSpringViewResolver bsvr = new BeetlSpringViewResolver();
		bsvr.setContentType("text/html;charset=UTF-8");
		bsvr.setSuffix(".html");
		bsvr.setConfig(configuration);
		bsvr.setGroupTemplate(configuration.getGroupTemplate());
		bsvr.setOrder(0);
		return bsvr;
	}
	
	@Bean(name="jspViewResolver")
	public InternalResourceViewResolver jspSpringViewResolver() {
		InternalResourceViewResolver jspViewResolver = new InternalResourceViewResolver();
		jspViewResolver.setViewClass(JstlView.class);
		jspViewResolver.setPrefix("/jsp");
		jspViewResolver.setSuffix(".jsp");
		jspViewResolver.setOrder(1); 
		return jspViewResolver;
	}
	
	@Bean
    public MultipartResolver getMultipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setMaxUploadSize(2 * 1024 * 1024); // 2MB
        return resolver;
    }
	
	/** 
     * 配置拦截器 
     * @param registry 
     */  
    public void addInterceptors(InterceptorRegistry registry) {  
        // registry.addInterceptor(new UserSecurityInterceptor()).addPathPatterns("/user/**");  
    }  
    
    @Bean(name = "druidWebStatFilter")
    public FilterRegistrationBean druidWebStatFilter() {
    	FilterRegistrationBean  bean = new FilterRegistrationBean();
    	WebStatFilter webStatFilter = new WebStatFilter();
		bean.setFilter(webStatFilter);
    	bean.addUrlPatterns("/*");
        return bean;
    }

    @Bean(name = "captchaServlet")
    public ServletRegistrationBean captchaServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        bean.setServlet(new CaptchaServlet());
        bean.addUrlMappings("/captcha");
        return bean;
    }
    
    @Bean(name = "statViewServlet")
    public ServletRegistrationBean statViewServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        bean.setServlet(new StatViewServlet());
        bean.addUrlMappings("/druid/*");
        return bean;
    }
}
