package in.hooray.shop.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "pay", ignoreUnknownFields = false)
@Component
public class PayProperties {
	
	private String merchantId;

	private String secret;
	
	private String payNotifyUrl;
	
	private String refundNotifyUrl;
	
	private String webReturnUrl;
	
	private String wapReturnUrl;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getPayNotifyUrl() {
		return payNotifyUrl;
	}

	public void setPayNotifyUrl(String payNotifyUrl) {
		this.payNotifyUrl = payNotifyUrl;
	}

	public String getRefundNotifyUrl() {
		return refundNotifyUrl;
	}

	public void setRefundNotifyUrl(String refundNotifyUrl) {
		this.refundNotifyUrl = refundNotifyUrl;
	}

	public String getWebReturnUrl() {
		return webReturnUrl;
	}

	public void setWebReturnUrl(String webReturnUrl) {
		this.webReturnUrl = webReturnUrl;
	}

	public String getWapReturnUrl() {
		return wapReturnUrl;
	}

	public void setWapReturnUrl(String wapReturnUrl) {
		this.wapReturnUrl = wapReturnUrl;
	}
	

}
