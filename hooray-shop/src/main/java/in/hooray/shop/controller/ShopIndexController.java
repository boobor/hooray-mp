package in.hooray.shop.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import in.hooray.service.shop.IMemberAccountService;

@Controller
public class ShopIndexController extends BaseController {

	@Autowired
	private IMemberAccountService memberAccountService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest req) {
		ModelAndView view = new ModelAndView();
		view.setViewName("/index");
	    //total 是模板的全局变量，可以直接访问
	    view.addObject("total", 1688);
	    return view;
	}
}
