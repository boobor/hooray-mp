package in.hooray.shop.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import in.hooray.shop.controller.BaseController;

/**
 * 用户提款相关
 * @author darking
 *
 */
@Controller
@RequestMapping(value = "/admin/member/withdraw")
public class MemberWithdrawManagerController extends BaseController {

}
