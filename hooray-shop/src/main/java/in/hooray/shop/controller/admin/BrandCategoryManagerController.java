package in.hooray.shop.controller.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;

import in.hooray.entity.shop.ShopBrandCategory;
import in.hooray.service.shop.IBrandService;
import in.hooray.shop.controller.BaseController;


/**
 * 品牌分类管理
 * http://www.cnblogs.com/HD/p/4107674.html
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/admin/brandCategory")
public class BrandCategoryManagerController extends BaseController {
	
	static final String BRAND_CATEGORY_INDEX_URI = "/admin/brandCategory"; 
	
	@Autowired
	IBrandService brandService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "";
	}
	
	/**
	 * 品牌分类添加、修改
	 * @param brandId
	 * @return
	 */
	@RequestMapping(value = "/edit/{categoryId}", method = RequestMethod.POST)
	public ModelAndView doEdit(@PathVariable("categoryId") Integer categoryId, HttpServletRequest request) {
		
		// ModelAndView mav = new ModelAndView(new RedirectView("manage.do "));
		ModelAndView mav = new ModelAndView(REDIRECT + BRAND_CATEGORY_INDEX_URI); 
		//编辑品牌分类 读取品牌分类信息
		ShopBrandCategory brandCategory = brandService.getBrandCategoryById(categoryId);
		if(brandCategory != null) {
			request.setAttribute("pageObj", brandCategory);
			return mav;
		} else {
			mav = new ModelAndView(FORWARD + "/edit/" + categoryId.toString());
		}
		
		return mav;
	}

	@RequestMapping(value = "/save.do", method = RequestMethod.POST)
	public String doSave(Integer id, Integer goods_category_id, String name) {
		Map<String, Object> categoryInfo = Maps.newHashMap();
		categoryInfo.put("name", name);
		categoryInfo.put("goods_category_id", goods_category_id);
		ShopBrandCategory sbc = new ShopBrandCategory();
		
		return "";
	}
	
	
}
