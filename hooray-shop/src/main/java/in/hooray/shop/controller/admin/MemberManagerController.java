package in.hooray.shop.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import in.hooray.shop.controller.BaseController;

/**
 * 后台会员模块
 * @author darking
 *
 */
@Controller
@RequestMapping(value = "/admin/member")
public class MemberManagerController extends BaseController{

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public void index() {
		
	}
	
	@RequestMapping(value = "/edit/", method = RequestMethod.GET)
	public void doEdit() {
		
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.GET)
	public void doSave() {
		
	}
	
	/*
	 * 彻底删除用户
	 */
	@RequestMapping(value = "/delete")
	public void doDelete() {
		
	}
	
	/**
	 * 用户余额管理
	 */
	@RequestMapping(value = "/balance/", method = RequestMethod.GET)
	public void doBalance() {
		
	}
	
	/**
	 * 删除到回收站
	 */
	@RequestMapping(value = "/reclaim/", method = RequestMethod.GET)
	public void doReclaim() {
		
	}
	
	/**
	 * 批量用户余额操作
	 */
	@RequestMapping(value = "/recharge/", method = RequestMethod.GET)
	public void doRecharge() {
		
	}
}
