package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_prop")
public class ShopProp implements Serializable {
    @Id
    private Integer id;

    /**
     * 道具名称
     */
    private String name;

    /**
     * 道具的卡号
     */
    @Column(name = "card_name")
    private String cardName;

    /**
     * 道具的密码
     */
    @Column(name = "card_pwd")
    private String cardPwd;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 面值
     */
    private BigDecimal value;

    /**
     * 道具类型 0:代金券
     */
    private Boolean type;

    /**
     * 条件数据 type=0时,表示ticket的表id,模型id
     */
    private String condition;

    /**
     * 是否关闭 0:正常,1:关闭,2:下订单未支付时临时锁定
     */
    @Column(name = "is_close")
    private Boolean isClose;

    /**
     * 道具图片
     */
    private String img;

    /**
     * 是否被使用过 0:未使用,1:已使用
     */
    @Column(name = "is_userd")
    private Boolean isUserd;

    /**
     * 是否被发送过 0:否 1:是
     */
    @Column(name = "is_send")
    private Boolean isSend;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取道具名称
     *
     * @return name - 道具名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置道具名称
     *
     * @param name 道具名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取道具的卡号
     *
     * @return card_name - 道具的卡号
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * 设置道具的卡号
     *
     * @param cardName 道具的卡号
     */
    public void setCardName(String cardName) {
        this.cardName = cardName == null ? null : cardName.trim();
    }

    /**
     * 获取道具的密码
     *
     * @return card_pwd - 道具的密码
     */
    public String getCardPwd() {
        return cardPwd;
    }

    /**
     * 设置道具的密码
     *
     * @param cardPwd 道具的密码
     */
    public void setCardPwd(String cardPwd) {
        this.cardPwd = cardPwd == null ? null : cardPwd.trim();
    }

    /**
     * 获取开始时间
     *
     * @return start_time - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取面值
     *
     * @return value - 面值
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * 设置面值
     *
     * @param value 面值
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * 获取道具类型 0:代金券
     *
     * @return type - 道具类型 0:代金券
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置道具类型 0:代金券
     *
     * @param type 道具类型 0:代金券
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取条件数据 type=0时,表示ticket的表id,模型id
     *
     * @return condition - 条件数据 type=0时,表示ticket的表id,模型id
     */
    public String getCondition() {
        return condition;
    }

    /**
     * 设置条件数据 type=0时,表示ticket的表id,模型id
     *
     * @param condition 条件数据 type=0时,表示ticket的表id,模型id
     */
    public void setCondition(String condition) {
        this.condition = condition == null ? null : condition.trim();
    }

    /**
     * 获取是否关闭 0:正常,1:关闭,2:下订单未支付时临时锁定
     *
     * @return is_close - 是否关闭 0:正常,1:关闭,2:下订单未支付时临时锁定
     */
    public Boolean getIsClose() {
        return isClose;
    }

    /**
     * 设置是否关闭 0:正常,1:关闭,2:下订单未支付时临时锁定
     *
     * @param isClose 是否关闭 0:正常,1:关闭,2:下订单未支付时临时锁定
     */
    public void setIsClose(Boolean isClose) {
        this.isClose = isClose;
    }

    /**
     * 获取道具图片
     *
     * @return img - 道具图片
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置道具图片
     *
     * @param img 道具图片
     */
    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    /**
     * 获取是否被使用过 0:未使用,1:已使用
     *
     * @return is_userd - 是否被使用过 0:未使用,1:已使用
     */
    public Boolean getIsUserd() {
        return isUserd;
    }

    /**
     * 设置是否被使用过 0:未使用,1:已使用
     *
     * @param isUserd 是否被使用过 0:未使用,1:已使用
     */
    public void setIsUserd(Boolean isUserd) {
        this.isUserd = isUserd;
    }

    /**
     * 获取是否被发送过 0:否 1:是
     *
     * @return is_send - 是否被发送过 0:否 1:是
     */
    public Boolean getIsSend() {
        return isSend;
    }

    /**
     * 设置是否被发送过 0:否 1:是
     *
     * @param isSend 是否被发送过 0:否 1:是
     */
    public void setIsSend(Boolean isSend) {
        this.isSend = isSend;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", cardName=").append(cardName);
        sb.append(", cardPwd=").append(cardPwd);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", value=").append(value);
        sb.append(", type=").append(type);
        sb.append(", condition=").append(condition);
        sb.append(", isClose=").append(isClose);
        sb.append(", img=").append(img);
        sb.append(", isUserd=").append(isUserd);
        sb.append(", isSend=").append(isSend);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}