package in.hooray.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_promotion")
public class ShopPromotion implements Serializable {
    @Id
    private Integer id;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 生效条件 type=0时为消费额度 type=1时为goods_id
     */
    private Integer condition;

    /**
     * 活动类型 0:购物车促销规则 1:商品限时抢购
     */
    private Boolean type;

    /**
     * 奖励值 type=0时奖励值 type=1时为抢购价格
     */
    @Column(name = "award_value")
    private String awardValue;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 奖励方式:0限时抢购 1减金额 2奖励折扣 3赠送积分 4赠送代金券 5赠送赠品 6免运费
     */
    @Column(name = "award_type")
    private Boolean awardType;

    /**
     * 是否关闭 0:否 1:是
     */
    @Column(name = "is_close")
    private Boolean isClose;

    /**
     * 活动介绍
     */
    private String intro;

    /**
     * 允许参与活动的用户组,all表示所有用户组
     */
    @Column(name = "user_group")
    private String userGroup;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取开始时间
     *
     * @return start_time - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取生效条件 type=0时为消费额度 type=1时为goods_id
     *
     * @return condition - 生效条件 type=0时为消费额度 type=1时为goods_id
     */
    public Integer getCondition() {
        return condition;
    }

    /**
     * 设置生效条件 type=0时为消费额度 type=1时为goods_id
     *
     * @param condition 生效条件 type=0时为消费额度 type=1时为goods_id
     */
    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    /**
     * 获取活动类型 0:购物车促销规则 1:商品限时抢购
     *
     * @return type - 活动类型 0:购物车促销规则 1:商品限时抢购
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置活动类型 0:购物车促销规则 1:商品限时抢购
     *
     * @param type 活动类型 0:购物车促销规则 1:商品限时抢购
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取奖励值 type=0时奖励值 type=1时为抢购价格
     *
     * @return award_value - 奖励值 type=0时奖励值 type=1时为抢购价格
     */
    public String getAwardValue() {
        return awardValue;
    }

    /**
     * 设置奖励值 type=0时奖励值 type=1时为抢购价格
     *
     * @param awardValue 奖励值 type=0时奖励值 type=1时为抢购价格
     */
    public void setAwardValue(String awardValue) {
        this.awardValue = awardValue == null ? null : awardValue.trim();
    }

    /**
     * 获取活动名称
     *
     * @return name - 活动名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置活动名称
     *
     * @param name 活动名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取奖励方式:0限时抢购 1减金额 2奖励折扣 3赠送积分 4赠送代金券 5赠送赠品 6免运费
     *
     * @return award_type - 奖励方式:0限时抢购 1减金额 2奖励折扣 3赠送积分 4赠送代金券 5赠送赠品 6免运费
     */
    public Boolean getAwardType() {
        return awardType;
    }

    /**
     * 设置奖励方式:0限时抢购 1减金额 2奖励折扣 3赠送积分 4赠送代金券 5赠送赠品 6免运费
     *
     * @param awardType 奖励方式:0限时抢购 1减金额 2奖励折扣 3赠送积分 4赠送代金券 5赠送赠品 6免运费
     */
    public void setAwardType(Boolean awardType) {
        this.awardType = awardType;
    }

    /**
     * 获取是否关闭 0:否 1:是
     *
     * @return is_close - 是否关闭 0:否 1:是
     */
    public Boolean getIsClose() {
        return isClose;
    }

    /**
     * 设置是否关闭 0:否 1:是
     *
     * @param isClose 是否关闭 0:否 1:是
     */
    public void setIsClose(Boolean isClose) {
        this.isClose = isClose;
    }

    /**
     * 获取活动介绍
     *
     * @return intro - 活动介绍
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 设置活动介绍
     *
     * @param intro 活动介绍
     */
    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    /**
     * 获取允许参与活动的用户组,all表示所有用户组
     *
     * @return user_group - 允许参与活动的用户组,all表示所有用户组
     */
    public String getUserGroup() {
        return userGroup;
    }

    /**
     * 设置允许参与活动的用户组,all表示所有用户组
     *
     * @param userGroup 允许参与活动的用户组,all表示所有用户组
     */
    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup == null ? null : userGroup.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", condition=").append(condition);
        sb.append(", type=").append(type);
        sb.append(", awardValue=").append(awardValue);
        sb.append(", name=").append(name);
        sb.append(", awardType=").append(awardType);
        sb.append(", isClose=").append(isClose);
        sb.append(", intro=").append(intro);
        sb.append(", userGroup=").append(userGroup);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}