package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_account_log")
public class AccountLog implements Serializable {
    @Id
    private Integer id;

    /**
     * 管理员ID
     */
    @Column(name = "admin_id")
    private Integer adminId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 0增加,1减少
     */
    private Boolean type;

    /**
     * 操作类型，意义请看accountLog类
     */
    private Byte event;

    /**
     * 发生时间
     */
    private Date time;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 每次增减后面的金额记录
     */
    @Column(name = "amount_log")
    private BigDecimal amountLog;

    /**
     * 备注
     */
    private String note;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取管理员ID
     *
     * @return admin_id - 管理员ID
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * 设置管理员ID
     *
     * @param adminId 管理员ID
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取0增加,1减少
     *
     * @return type - 0增加,1减少
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置0增加,1减少
     *
     * @param type 0增加,1减少
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取操作类型，意义请看accountLog类
     *
     * @return event - 操作类型，意义请看accountLog类
     */
    public Byte getEvent() {
        return event;
    }

    /**
     * 设置操作类型，意义请看accountLog类
     *
     * @param event 操作类型，意义请看accountLog类
     */
    public void setEvent(Byte event) {
        this.event = event;
    }

    /**
     * 获取发生时间
     *
     * @return time - 发生时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置发生时间
     *
     * @param time 发生时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取每次增减后面的金额记录
     *
     * @return amount_log - 每次增减后面的金额记录
     */
    public BigDecimal getAmountLog() {
        return amountLog;
    }

    /**
     * 设置每次增减后面的金额记录
     *
     * @param amountLog 每次增减后面的金额记录
     */
    public void setAmountLog(BigDecimal amountLog) {
        this.amountLog = amountLog;
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", adminId=").append(adminId);
        sb.append(", userId=").append(userId);
        sb.append(", type=").append(type);
        sb.append(", event=").append(event);
        sb.append(", time=").append(time);
        sb.append(", amount=").append(amount);
        sb.append(", amountLog=").append(amountLog);
        sb.append(", note=").append(note);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}