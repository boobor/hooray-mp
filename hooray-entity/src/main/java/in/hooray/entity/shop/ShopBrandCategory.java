package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_brand_category")
public class ShopBrandCategory implements Serializable {
    /**
     * 分类ID
     */
    @Id
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 商品分类ID
     */
    @Column(name = "goods_category_id")
    private Integer goodsCategoryId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取分类ID
     *
     * @return id - 分类ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置分类ID
     *
     * @param id 分类ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取分类名称
     *
     * @return name - 分类名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置分类名称
     *
     * @param name 分类名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取商品分类ID
     *
     * @return goods_category_id - 商品分类ID
     */
    public Integer getGoodsCategoryId() {
        return goodsCategoryId;
    }

    /**
     * 设置商品分类ID
     *
     * @param goodsCategoryId 商品分类ID
     */
    public void setGoodsCategoryId(Integer goodsCategoryId) {
        this.goodsCategoryId = goodsCategoryId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", goodsCategoryId=").append(goodsCategoryId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}