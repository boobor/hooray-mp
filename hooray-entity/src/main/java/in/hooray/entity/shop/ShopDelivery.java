package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "shop_delivery")
public class ShopDelivery implements Serializable {
    @Id
    private Integer id;

    /**
     * 快递名称
     */
    private String name;

    /**
     * 快递描述
     */
    private String description;

    /**
     * 配送类型 0先付款后发货 1先发货后付款 2自提点
     */
    private Boolean type;

    /**
     * 首重重量(克)
     */
    @Column(name = "first_weight")
    private Integer firstWeight;

    /**
     * 续重重量(克)
     */
    @Column(name = "second_weight")
    private Integer secondWeight;

    /**
     * 首重价格
     */
    @Column(name = "first_price")
    private BigDecimal firstPrice;

    /**
     * 续重价格
     */
    @Column(name = "second_price")
    private BigDecimal secondPrice;

    /**
     * 开启状态
     */
    private Boolean status;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 是否支持物流保价 1支持保价 0  不支持保价
     */
    @Column(name = "is_save_price")
    private Boolean isSavePrice;

    /**
     * 保价费率
     */
    @Column(name = "save_rate")
    private BigDecimal saveRate;

    /**
     * 最低保价
     */
    @Column(name = "low_price")
    private BigDecimal lowPrice;

    /**
     * 费用类型 0统一设置 1指定地区费用
     */
    @Column(name = "price_type")
    private Boolean priceType;

    /**
     * 启用默认费用 1启用 0 不启用
     */
    @Column(name = "open_default")
    private Boolean openDefault;

    /**
     * 是否删除 0:未删除 1:删除
     */
    @Column(name = "is_delete")
    private Boolean isDelete;

    /**
     * 配送区域id
     */
    @Column(name = "area_groupid")
    private String areaGroupid;

    /**
     * 配送地址对应的首重价格
     */
    private String firstprice;

    /**
     * 配送地区对应的续重价格
     */
    private String secondprice;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取快递名称
     *
     * @return name - 快递名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置快递名称
     *
     * @param name 快递名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取快递描述
     *
     * @return description - 快递描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置快递描述
     *
     * @param description 快递描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 获取配送类型 0先付款后发货 1先发货后付款 2自提点
     *
     * @return type - 配送类型 0先付款后发货 1先发货后付款 2自提点
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置配送类型 0先付款后发货 1先发货后付款 2自提点
     *
     * @param type 配送类型 0先付款后发货 1先发货后付款 2自提点
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取首重重量(克)
     *
     * @return first_weight - 首重重量(克)
     */
    public Integer getFirstWeight() {
        return firstWeight;
    }

    /**
     * 设置首重重量(克)
     *
     * @param firstWeight 首重重量(克)
     */
    public void setFirstWeight(Integer firstWeight) {
        this.firstWeight = firstWeight;
    }

    /**
     * 获取续重重量(克)
     *
     * @return second_weight - 续重重量(克)
     */
    public Integer getSecondWeight() {
        return secondWeight;
    }

    /**
     * 设置续重重量(克)
     *
     * @param secondWeight 续重重量(克)
     */
    public void setSecondWeight(Integer secondWeight) {
        this.secondWeight = secondWeight;
    }

    /**
     * 获取首重价格
     *
     * @return first_price - 首重价格
     */
    public BigDecimal getFirstPrice() {
        return firstPrice;
    }

    /**
     * 设置首重价格
     *
     * @param firstPrice 首重价格
     */
    public void setFirstPrice(BigDecimal firstPrice) {
        this.firstPrice = firstPrice;
    }

    /**
     * 获取续重价格
     *
     * @return second_price - 续重价格
     */
    public BigDecimal getSecondPrice() {
        return secondPrice;
    }

    /**
     * 设置续重价格
     *
     * @param secondPrice 续重价格
     */
    public void setSecondPrice(BigDecimal secondPrice) {
        this.secondPrice = secondPrice;
    }

    /**
     * 获取开启状态
     *
     * @return status - 开启状态
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置开启状态
     *
     * @param status 开启状态
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    /**
     * 获取是否支持物流保价 1支持保价 0  不支持保价
     *
     * @return is_save_price - 是否支持物流保价 1支持保价 0  不支持保价
     */
    public Boolean getIsSavePrice() {
        return isSavePrice;
    }

    /**
     * 设置是否支持物流保价 1支持保价 0  不支持保价
     *
     * @param isSavePrice 是否支持物流保价 1支持保价 0  不支持保价
     */
    public void setIsSavePrice(Boolean isSavePrice) {
        this.isSavePrice = isSavePrice;
    }

    /**
     * 获取保价费率
     *
     * @return save_rate - 保价费率
     */
    public BigDecimal getSaveRate() {
        return saveRate;
    }

    /**
     * 设置保价费率
     *
     * @param saveRate 保价费率
     */
    public void setSaveRate(BigDecimal saveRate) {
        this.saveRate = saveRate;
    }

    /**
     * 获取最低保价
     *
     * @return low_price - 最低保价
     */
    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    /**
     * 设置最低保价
     *
     * @param lowPrice 最低保价
     */
    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    /**
     * 获取费用类型 0统一设置 1指定地区费用
     *
     * @return price_type - 费用类型 0统一设置 1指定地区费用
     */
    public Boolean getPriceType() {
        return priceType;
    }

    /**
     * 设置费用类型 0统一设置 1指定地区费用
     *
     * @param priceType 费用类型 0统一设置 1指定地区费用
     */
    public void setPriceType(Boolean priceType) {
        this.priceType = priceType;
    }

    /**
     * 获取启用默认费用 1启用 0 不启用
     *
     * @return open_default - 启用默认费用 1启用 0 不启用
     */
    public Boolean getOpenDefault() {
        return openDefault;
    }

    /**
     * 设置启用默认费用 1启用 0 不启用
     *
     * @param openDefault 启用默认费用 1启用 0 不启用
     */
    public void setOpenDefault(Boolean openDefault) {
        this.openDefault = openDefault;
    }

    /**
     * 获取是否删除 0:未删除 1:删除
     *
     * @return is_delete - 是否删除 0:未删除 1:删除
     */
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除 0:未删除 1:删除
     *
     * @param isDelete 是否删除 0:未删除 1:删除
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取配送区域id
     *
     * @return area_groupid - 配送区域id
     */
    public String getAreaGroupid() {
        return areaGroupid;
    }

    /**
     * 设置配送区域id
     *
     * @param areaGroupid 配送区域id
     */
    public void setAreaGroupid(String areaGroupid) {
        this.areaGroupid = areaGroupid == null ? null : areaGroupid.trim();
    }

    /**
     * 获取配送地址对应的首重价格
     *
     * @return firstprice - 配送地址对应的首重价格
     */
    public String getFirstprice() {
        return firstprice;
    }

    /**
     * 设置配送地址对应的首重价格
     *
     * @param firstprice 配送地址对应的首重价格
     */
    public void setFirstprice(String firstprice) {
        this.firstprice = firstprice == null ? null : firstprice.trim();
    }

    /**
     * 获取配送地区对应的续重价格
     *
     * @return secondprice - 配送地区对应的续重价格
     */
    public String getSecondprice() {
        return secondprice;
    }

    /**
     * 设置配送地区对应的续重价格
     *
     * @param secondprice 配送地区对应的续重价格
     */
    public void setSecondprice(String secondprice) {
        this.secondprice = secondprice == null ? null : secondprice.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", type=").append(type);
        sb.append(", firstWeight=").append(firstWeight);
        sb.append(", secondWeight=").append(secondWeight);
        sb.append(", firstPrice=").append(firstPrice);
        sb.append(", secondPrice=").append(secondPrice);
        sb.append(", status=").append(status);
        sb.append(", sort=").append(sort);
        sb.append(", isSavePrice=").append(isSavePrice);
        sb.append(", saveRate=").append(saveRate);
        sb.append(", lowPrice=").append(lowPrice);
        sb.append(", priceType=").append(priceType);
        sb.append(", openDefault=").append(openDefault);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", areaGroupid=").append(areaGroupid);
        sb.append(", firstprice=").append(firstprice);
        sb.append(", secondprice=").append(secondprice);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}