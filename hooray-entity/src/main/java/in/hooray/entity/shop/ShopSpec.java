package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_spec")
public class ShopSpec implements Serializable {
    @Id
    private Integer id;

    /**
     * 规格名称
     */
    private String name;

    /**
     * 显示类型 1文字 2图片
     */
    private Boolean type;

    /**
     * 备注说明
     */
    private String note;

    /**
     * 是否删除1删除
     */
    @Column(name = "is_del")
    private Boolean isDel;

    /**
     * 商家ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;

    /**
     * 规格值
     */
    private String value;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取规格名称
     *
     * @return name - 规格名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置规格名称
     *
     * @param name 规格名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取显示类型 1文字 2图片
     *
     * @return type - 显示类型 1文字 2图片
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置显示类型 1文字 2图片
     *
     * @param type 显示类型 1文字 2图片
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取备注说明
     *
     * @return note - 备注说明
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注说明
     *
     * @param note 备注说明
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * 获取是否删除1删除
     *
     * @return is_del - 是否删除1删除
     */
    public Boolean getIsDel() {
        return isDel;
    }

    /**
     * 设置是否删除1删除
     *
     * @param isDel 是否删除1删除
     */
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    /**
     * 获取商家ID
     *
     * @return seller_id - 商家ID
     */
    public Integer getSellerId() {
        return sellerId;
    }

    /**
     * 设置商家ID
     *
     * @param sellerId 商家ID
     */
    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * 获取规格值
     *
     * @return value - 规格值
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置规格值
     *
     * @param value 规格值
     */
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append(", note=").append(note);
        sb.append(", isDel=").append(isDel);
        sb.append(", sellerId=").append(sellerId);
        sb.append(", value=").append(value);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}