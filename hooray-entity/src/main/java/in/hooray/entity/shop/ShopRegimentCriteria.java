package in.hooray.entity.shop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShopRegimentCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ShopRegimentCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andStoreNumsIsNull() {
            addCriterion("store_nums is null");
            return (Criteria) this;
        }

        public Criteria andStoreNumsIsNotNull() {
            addCriterion("store_nums is not null");
            return (Criteria) this;
        }

        public Criteria andStoreNumsEqualTo(Integer value) {
            addCriterion("store_nums =", value, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsNotEqualTo(Integer value) {
            addCriterion("store_nums <>", value, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsGreaterThan(Integer value) {
            addCriterion("store_nums >", value, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsGreaterThanOrEqualTo(Integer value) {
            addCriterion("store_nums >=", value, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsLessThan(Integer value) {
            addCriterion("store_nums <", value, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsLessThanOrEqualTo(Integer value) {
            addCriterion("store_nums <=", value, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsIn(List<Integer> values) {
            addCriterion("store_nums in", values, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsNotIn(List<Integer> values) {
            addCriterion("store_nums not in", values, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsBetween(Integer value1, Integer value2) {
            addCriterion("store_nums between", value1, value2, "storeNums");
            return (Criteria) this;
        }

        public Criteria andStoreNumsNotBetween(Integer value1, Integer value2) {
            addCriterion("store_nums not between", value1, value2, "storeNums");
            return (Criteria) this;
        }

        public Criteria andSumCountIsNull() {
            addCriterion("sum_count is null");
            return (Criteria) this;
        }

        public Criteria andSumCountIsNotNull() {
            addCriterion("sum_count is not null");
            return (Criteria) this;
        }

        public Criteria andSumCountEqualTo(Integer value) {
            addCriterion("sum_count =", value, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountNotEqualTo(Integer value) {
            addCriterion("sum_count <>", value, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountGreaterThan(Integer value) {
            addCriterion("sum_count >", value, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_count >=", value, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountLessThan(Integer value) {
            addCriterion("sum_count <", value, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountLessThanOrEqualTo(Integer value) {
            addCriterion("sum_count <=", value, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountIn(List<Integer> values) {
            addCriterion("sum_count in", values, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountNotIn(List<Integer> values) {
            addCriterion("sum_count not in", values, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountBetween(Integer value1, Integer value2) {
            addCriterion("sum_count between", value1, value2, "sumCount");
            return (Criteria) this;
        }

        public Criteria andSumCountNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_count not between", value1, value2, "sumCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountIsNull() {
            addCriterion("limit_min_count is null");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountIsNotNull() {
            addCriterion("limit_min_count is not null");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountEqualTo(Integer value) {
            addCriterion("limit_min_count =", value, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountNotEqualTo(Integer value) {
            addCriterion("limit_min_count <>", value, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountGreaterThan(Integer value) {
            addCriterion("limit_min_count >", value, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("limit_min_count >=", value, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountLessThan(Integer value) {
            addCriterion("limit_min_count <", value, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountLessThanOrEqualTo(Integer value) {
            addCriterion("limit_min_count <=", value, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountIn(List<Integer> values) {
            addCriterion("limit_min_count in", values, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountNotIn(List<Integer> values) {
            addCriterion("limit_min_count not in", values, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountBetween(Integer value1, Integer value2) {
            addCriterion("limit_min_count between", value1, value2, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMinCountNotBetween(Integer value1, Integer value2) {
            addCriterion("limit_min_count not between", value1, value2, "limitMinCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountIsNull() {
            addCriterion("limit_max_count is null");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountIsNotNull() {
            addCriterion("limit_max_count is not null");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountEqualTo(Integer value) {
            addCriterion("limit_max_count =", value, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountNotEqualTo(Integer value) {
            addCriterion("limit_max_count <>", value, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountGreaterThan(Integer value) {
            addCriterion("limit_max_count >", value, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("limit_max_count >=", value, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountLessThan(Integer value) {
            addCriterion("limit_max_count <", value, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountLessThanOrEqualTo(Integer value) {
            addCriterion("limit_max_count <=", value, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountIn(List<Integer> values) {
            addCriterion("limit_max_count in", values, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountNotIn(List<Integer> values) {
            addCriterion("limit_max_count not in", values, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountBetween(Integer value1, Integer value2) {
            addCriterion("limit_max_count between", value1, value2, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andLimitMaxCountNotBetween(Integer value1, Integer value2) {
            addCriterion("limit_max_count not between", value1, value2, "limitMaxCount");
            return (Criteria) this;
        }

        public Criteria andIntroIsNull() {
            addCriterion("intro is null");
            return (Criteria) this;
        }

        public Criteria andIntroIsNotNull() {
            addCriterion("intro is not null");
            return (Criteria) this;
        }

        public Criteria andIntroEqualTo(String value) {
            addCriterion("intro =", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotEqualTo(String value) {
            addCriterion("intro <>", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroGreaterThan(String value) {
            addCriterion("intro >", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroGreaterThanOrEqualTo(String value) {
            addCriterion("intro >=", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroLessThan(String value) {
            addCriterion("intro <", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroLessThanOrEqualTo(String value) {
            addCriterion("intro <=", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroLike(String value) {
            addCriterion("intro like", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotLike(String value) {
            addCriterion("intro not like", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroIn(List<String> values) {
            addCriterion("intro in", values, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotIn(List<String> values) {
            addCriterion("intro not in", values, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroBetween(String value1, String value2) {
            addCriterion("intro between", value1, value2, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotBetween(String value1, String value2) {
            addCriterion("intro not between", value1, value2, "intro");
            return (Criteria) this;
        }

        public Criteria andIsCloseIsNull() {
            addCriterion("is_close is null");
            return (Criteria) this;
        }

        public Criteria andIsCloseIsNotNull() {
            addCriterion("is_close is not null");
            return (Criteria) this;
        }

        public Criteria andIsCloseEqualTo(Boolean value) {
            addCriterion("is_close =", value, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseNotEqualTo(Boolean value) {
            addCriterion("is_close <>", value, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseGreaterThan(Boolean value) {
            addCriterion("is_close >", value, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_close >=", value, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseLessThan(Boolean value) {
            addCriterion("is_close <", value, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseLessThanOrEqualTo(Boolean value) {
            addCriterion("is_close <=", value, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseIn(List<Boolean> values) {
            addCriterion("is_close in", values, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseNotIn(List<Boolean> values) {
            addCriterion("is_close not in", values, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseBetween(Boolean value1, Boolean value2) {
            addCriterion("is_close between", value1, value2, "isClose");
            return (Criteria) this;
        }

        public Criteria andIsCloseNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_close not between", value1, value2, "isClose");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceIsNull() {
            addCriterion("regiment_price is null");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceIsNotNull() {
            addCriterion("regiment_price is not null");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceEqualTo(BigDecimal value) {
            addCriterion("regiment_price =", value, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceNotEqualTo(BigDecimal value) {
            addCriterion("regiment_price <>", value, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceGreaterThan(BigDecimal value) {
            addCriterion("regiment_price >", value, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("regiment_price >=", value, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceLessThan(BigDecimal value) {
            addCriterion("regiment_price <", value, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("regiment_price <=", value, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceIn(List<BigDecimal> values) {
            addCriterion("regiment_price in", values, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceNotIn(List<BigDecimal> values) {
            addCriterion("regiment_price not in", values, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("regiment_price between", value1, value2, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andRegimentPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("regiment_price not between", value1, value2, "regimentPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceIsNull() {
            addCriterion("sell_price is null");
            return (Criteria) this;
        }

        public Criteria andSellPriceIsNotNull() {
            addCriterion("sell_price is not null");
            return (Criteria) this;
        }

        public Criteria andSellPriceEqualTo(BigDecimal value) {
            addCriterion("sell_price =", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceNotEqualTo(BigDecimal value) {
            addCriterion("sell_price <>", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceGreaterThan(BigDecimal value) {
            addCriterion("sell_price >", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("sell_price >=", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceLessThan(BigDecimal value) {
            addCriterion("sell_price <", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("sell_price <=", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceIn(List<BigDecimal> values) {
            addCriterion("sell_price in", values, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceNotIn(List<BigDecimal> values) {
            addCriterion("sell_price not in", values, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sell_price between", value1, value2, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sell_price not between", value1, value2, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNull() {
            addCriterion("goods_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNotNull() {
            addCriterion("goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdEqualTo(Integer value) {
            addCriterion("goods_id =", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotEqualTo(Integer value) {
            addCriterion("goods_id <>", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThan(Integer value) {
            addCriterion("goods_id >", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goods_id >=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThan(Integer value) {
            addCriterion("goods_id <", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThanOrEqualTo(Integer value) {
            addCriterion("goods_id <=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIn(List<Integer> values) {
            addCriterion("goods_id in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotIn(List<Integer> values) {
            addCriterion("goods_id not in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdBetween(Integer value1, Integer value2) {
            addCriterion("goods_id between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goods_id not between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andImgIsNull() {
            addCriterion("img is null");
            return (Criteria) this;
        }

        public Criteria andImgIsNotNull() {
            addCriterion("img is not null");
            return (Criteria) this;
        }

        public Criteria andImgEqualTo(String value) {
            addCriterion("img =", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotEqualTo(String value) {
            addCriterion("img <>", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgGreaterThan(String value) {
            addCriterion("img >", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgGreaterThanOrEqualTo(String value) {
            addCriterion("img >=", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLessThan(String value) {
            addCriterion("img <", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLessThanOrEqualTo(String value) {
            addCriterion("img <=", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLike(String value) {
            addCriterion("img like", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotLike(String value) {
            addCriterion("img not like", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgIn(List<String> values) {
            addCriterion("img in", values, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotIn(List<String> values) {
            addCriterion("img not in", values, "img");
            return (Criteria) this;
        }

        public Criteria andImgBetween(String value1, String value2) {
            addCriterion("img between", value1, value2, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotBetween(String value1, String value2) {
            addCriterion("img not between", value1, value2, "img");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Short value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Short value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Short value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Short value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Short value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Short value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Short> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Short> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Short value1, Short value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Short value1, Short value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andTitleLikeInsensitive(String value) {
            addCriterion("upper(title) like", value.toUpperCase(), "title");
            return (Criteria) this;
        }

        public Criteria andIntroLikeInsensitive(String value) {
            addCriterion("upper(intro) like", value.toUpperCase(), "intro");
            return (Criteria) this;
        }

        public Criteria andImgLikeInsensitive(String value) {
            addCriterion("upper(img) like", value.toUpperCase(), "img");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}