package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_online_recharge")
public class ShopOnlineRecharge implements Serializable {
    @Id
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 充值单号
     */
    @Column(name = "recharge_no")
    private String rechargeNo;

    /**
     * 充值金额
     */
    private BigDecimal account;

    /**
     * 时间
     */
    private Date time;

    /**
     * 充值方式名称
     */
    @Column(name = "payment_name")
    private String paymentName;

    /**
     * 充值状态 0:未成功 1:充值成功
     */
    private Boolean status;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取充值单号
     *
     * @return recharge_no - 充值单号
     */
    public String getRechargeNo() {
        return rechargeNo;
    }

    /**
     * 设置充值单号
     *
     * @param rechargeNo 充值单号
     */
    public void setRechargeNo(String rechargeNo) {
        this.rechargeNo = rechargeNo == null ? null : rechargeNo.trim();
    }

    /**
     * 获取充值金额
     *
     * @return account - 充值金额
     */
    public BigDecimal getAccount() {
        return account;
    }

    /**
     * 设置充值金额
     *
     * @param account 充值金额
     */
    public void setAccount(BigDecimal account) {
        this.account = account;
    }

    /**
     * 获取时间
     *
     * @return time - 时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取充值方式名称
     *
     * @return payment_name - 充值方式名称
     */
    public String getPaymentName() {
        return paymentName;
    }

    /**
     * 设置充值方式名称
     *
     * @param paymentName 充值方式名称
     */
    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName == null ? null : paymentName.trim();
    }

    /**
     * 获取充值状态 0:未成功 1:充值成功
     *
     * @return status - 充值状态 0:未成功 1:充值成功
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置充值状态 0:未成功 1:充值成功
     *
     * @param status 充值状态 0:未成功 1:充值成功
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", rechargeNo=").append(rechargeNo);
        sb.append(", account=").append(account);
        sb.append(", time=").append(time);
        sb.append(", paymentName=").append(paymentName);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}