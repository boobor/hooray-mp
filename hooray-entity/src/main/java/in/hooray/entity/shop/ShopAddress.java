package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_address")
public class ShopAddress implements Serializable {
    @Id
    private Integer id;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 收货人姓名
     */
    @Column(name = "accept_name")
    private String acceptName;

    /**
     * 邮编
     */
    private String zip;

    /**
     * 联系电话
     */
    private String telphone;

    /**
     * 国ID
     */
    private Integer country;

    /**
     * 省ID
     */
    private Integer province;

    /**
     * 市ID
     */
    private Integer city;

    /**
     * 区ID
     */
    private Integer area;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 是否默认,0:为非默认,1:默认
     */
    private Boolean is_default;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取收货人姓名
     *
     * @return accept_name - 收货人姓名
     */
    public String getAcceptName() {
        return acceptName;
    }

    /**
     * 设置收货人姓名
     *
     * @param acceptName 收货人姓名
     */
    public void setAcceptName(String acceptName) {
        this.acceptName = acceptName == null ? null : acceptName.trim();
    }

    /**
     * 获取邮编
     *
     * @return zip - 邮编
     */
    public String getZip() {
        return zip;
    }

    /**
     * 设置邮编
     *
     * @param zip 邮编
     */
    public void setZip(String zip) {
        this.zip = zip == null ? null : zip.trim();
    }

    /**
     * 获取联系电话
     *
     * @return telphone - 联系电话
     */
    public String getTelphone() {
        return telphone;
    }

    /**
     * 设置联系电话
     *
     * @param telphone 联系电话
     */
    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    /**
     * 获取国ID
     *
     * @return country - 国ID
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * 设置国ID
     *
     * @param country 国ID
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * 获取省ID
     *
     * @return province - 省ID
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 设置省ID
     *
     * @param province 省ID
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * 获取市ID
     *
     * @return city - 市ID
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 设置市ID
     *
     * @param city 市ID
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * 获取区ID
     *
     * @return area - 区ID
     */
    public Integer getArea() {
        return area;
    }

    /**
     * 设置区ID
     *
     * @param area 区ID
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * 获取收货地址
     *
     * @return address - 收货地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置收货地址
     *
     * @param address 收货地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取手机
     *
     * @return mobile - 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机
     *
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取是否默认,0:为非默认,1:默认
     *
     * @return default - 是否默认,0:为非默认,1:默认
     */
    public Boolean isDefault() {
        return is_default;
    }

    /**
     * 设置是否默认,0:为非默认,1:默认
     *
     * @param default 是否默认,0:为非默认,1:默认
     */
    public void setDefault(Boolean is_default) {
        this.is_default = is_default;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", acceptName=").append(acceptName);
        sb.append(", zip=").append(zip);
        sb.append(", telphone=").append(telphone);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", area=").append(area);
        sb.append(", address=").append(address);
        sb.append(", mobile=").append(mobile);
        sb.append(", default=").append(is_default);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}