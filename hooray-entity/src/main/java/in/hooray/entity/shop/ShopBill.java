package in.hooray.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_bill")
public class ShopBill implements Serializable {
    @Id
    private Integer id;

    /**
     * 商家ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;

    /**
     * 申请结算时间
     */
    @Column(name = "apply_time")
    private Date applyTime;

    /**
     * 支付结算时间
     */
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * 管理员ID
     */
    @Column(name = "admin_id")
    private Integer adminId;

    /**
     * 0:未结算,1:已结算
     */
    @Column(name = "is_pay")
    private Boolean isPay;

    /**
     * 结算起始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 结算终止时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 申请结算文本
     */
    @Column(name = "apply_content")
    private String applyContent;

    /**
     * 支付结算文本
     */
    @Column(name = "pay_content")
    private String payContent;

    /**
     * 结算明细
     */
    private String log;

    /**
     * order_goods表主键ID，结算的ID
     */
    @Column(name = "order_goods_ids")
    private String orderGoodsIds;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商家ID
     *
     * @return seller_id - 商家ID
     */
    public Integer getSellerId() {
        return sellerId;
    }

    /**
     * 设置商家ID
     *
     * @param sellerId 商家ID
     */
    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * 获取申请结算时间
     *
     * @return apply_time - 申请结算时间
     */
    public Date getApplyTime() {
        return applyTime;
    }

    /**
     * 设置申请结算时间
     *
     * @param applyTime 申请结算时间
     */
    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    /**
     * 获取支付结算时间
     *
     * @return pay_time - 支付结算时间
     */
    public Date getPayTime() {
        return payTime;
    }

    /**
     * 设置支付结算时间
     *
     * @param payTime 支付结算时间
     */
    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    /**
     * 获取管理员ID
     *
     * @return admin_id - 管理员ID
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * 设置管理员ID
     *
     * @param adminId 管理员ID
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * 获取0:未结算,1:已结算
     *
     * @return is_pay - 0:未结算,1:已结算
     */
    public Boolean getIsPay() {
        return isPay;
    }

    /**
     * 设置0:未结算,1:已结算
     *
     * @param isPay 0:未结算,1:已结算
     */
    public void setIsPay(Boolean isPay) {
        this.isPay = isPay;
    }

    /**
     * 获取结算起始时间
     *
     * @return start_time - 结算起始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置结算起始时间
     *
     * @param startTime 结算起始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结算终止时间
     *
     * @return end_time - 结算终止时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结算终止时间
     *
     * @param endTime 结算终止时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取申请结算文本
     *
     * @return apply_content - 申请结算文本
     */
    public String getApplyContent() {
        return applyContent;
    }

    /**
     * 设置申请结算文本
     *
     * @param applyContent 申请结算文本
     */
    public void setApplyContent(String applyContent) {
        this.applyContent = applyContent == null ? null : applyContent.trim();
    }

    /**
     * 获取支付结算文本
     *
     * @return pay_content - 支付结算文本
     */
    public String getPayContent() {
        return payContent;
    }

    /**
     * 设置支付结算文本
     *
     * @param payContent 支付结算文本
     */
    public void setPayContent(String payContent) {
        this.payContent = payContent == null ? null : payContent.trim();
    }

    /**
     * 获取结算明细
     *
     * @return log - 结算明细
     */
    public String getLog() {
        return log;
    }

    /**
     * 设置结算明细
     *
     * @param log 结算明细
     */
    public void setLog(String log) {
        this.log = log == null ? null : log.trim();
    }

    /**
     * 获取order_goods表主键ID，结算的ID
     *
     * @return order_goods_ids - order_goods表主键ID，结算的ID
     */
    public String getOrderGoodsIds() {
        return orderGoodsIds;
    }

    /**
     * 设置order_goods表主键ID，结算的ID
     *
     * @param orderGoodsIds order_goods表主键ID，结算的ID
     */
    public void setOrderGoodsIds(String orderGoodsIds) {
        this.orderGoodsIds = orderGoodsIds == null ? null : orderGoodsIds.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", sellerId=").append(sellerId);
        sb.append(", applyTime=").append(applyTime);
        sb.append(", payTime=").append(payTime);
        sb.append(", adminId=").append(adminId);
        sb.append(", isPay=").append(isPay);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", applyContent=").append(applyContent);
        sb.append(", payContent=").append(payContent);
        sb.append(", log=").append(log);
        sb.append(", orderGoodsIds=").append(orderGoodsIds);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}