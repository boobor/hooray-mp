package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "shop_group_price")
public class ShopGroupPrice implements Serializable {
    @Id
    private Integer id;

    /**
     * 产品ID
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 货品ID
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 用户组ID
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 价格
     */
    private BigDecimal price;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取产品ID
     *
     * @return goods_id - 产品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置产品ID
     *
     * @param goodsId 产品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取货品ID
     *
     * @return product_id - 货品ID
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * 设置货品ID
     *
     * @param productId 货品ID
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * 获取用户组ID
     *
     * @return group_id - 用户组ID
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * 设置用户组ID
     *
     * @param groupId 用户组ID
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取价格
     *
     * @return price - 价格
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 设置价格
     *
     * @param price 价格
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", productId=").append(productId);
        sb.append(", groupId=").append(groupId);
        sb.append(", price=").append(price);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}