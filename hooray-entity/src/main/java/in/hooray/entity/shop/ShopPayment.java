package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "shop_payment")
public class ShopPayment implements Serializable {
    @Id
    private Integer id;

    /**
     * 支付名称
     */
    private String name;

    /**
     * 1:线上、2:线下
     */
    private Boolean type;

    /**
     * 支付类名称
     */
    @Column(name = "class_name")
    private String className;

    /**
     * 支付方式logo图片路径
     */
    private String logo;

    /**
     * 安装状态 0启用 1禁用
     */
    private Boolean status;

    /**
     * 排序
     */
    private Short order;

    /**
     * 手续费
     */
    private BigDecimal poundage;

    /**
     * 手续费方式 1百分比 2固定值
     */
    @Column(name = "poundage_type")
    private Boolean poundageType;

    /**
     * 1:PC端 2:移动端 3:通用
     */
    @Column(name = "client_type")
    private Boolean clientType;

    /**
     * 描述
     */
    private String description;

    /**
     * 支付说明
     */
    private String note;

    /**
     * 配置参数,json数据对象
     */
    @Column(name = "config_param")
    private String configParam;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取支付名称
     *
     * @return name - 支付名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置支付名称
     *
     * @param name 支付名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取1:线上、2:线下
     *
     * @return type - 1:线上、2:线下
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置1:线上、2:线下
     *
     * @param type 1:线上、2:线下
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取支付类名称
     *
     * @return class_name - 支付类名称
     */
    public String getClassName() {
        return className;
    }

    /**
     * 设置支付类名称
     *
     * @param className 支付类名称
     */
    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    /**
     * 获取支付方式logo图片路径
     *
     * @return logo - 支付方式logo图片路径
     */
    public String getLogo() {
        return logo;
    }

    /**
     * 设置支付方式logo图片路径
     *
     * @param logo 支付方式logo图片路径
     */
    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    /**
     * 获取安装状态 0启用 1禁用
     *
     * @return status - 安装状态 0启用 1禁用
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置安装状态 0启用 1禁用
     *
     * @param status 安装状态 0启用 1禁用
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取排序
     *
     * @return order - 排序
     */
    public Short getOrder() {
        return order;
    }

    /**
     * 设置排序
     *
     * @param order 排序
     */
    public void setOrder(Short order) {
        this.order = order;
    }

    /**
     * 获取手续费
     *
     * @return poundage - 手续费
     */
    public BigDecimal getPoundage() {
        return poundage;
    }

    /**
     * 设置手续费
     *
     * @param poundage 手续费
     */
    public void setPoundage(BigDecimal poundage) {
        this.poundage = poundage;
    }

    /**
     * 获取手续费方式 1百分比 2固定值
     *
     * @return poundage_type - 手续费方式 1百分比 2固定值
     */
    public Boolean getPoundageType() {
        return poundageType;
    }

    /**
     * 设置手续费方式 1百分比 2固定值
     *
     * @param poundageType 手续费方式 1百分比 2固定值
     */
    public void setPoundageType(Boolean poundageType) {
        this.poundageType = poundageType;
    }

    /**
     * 获取1:PC端 2:移动端 3:通用
     *
     * @return client_type - 1:PC端 2:移动端 3:通用
     */
    public Boolean getClientType() {
        return clientType;
    }

    /**
     * 设置1:PC端 2:移动端 3:通用
     *
     * @param clientType 1:PC端 2:移动端 3:通用
     */
    public void setClientType(Boolean clientType) {
        this.clientType = clientType;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 获取支付说明
     *
     * @return note - 支付说明
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置支付说明
     *
     * @param note 支付说明
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * 获取配置参数,json数据对象
     *
     * @return config_param - 配置参数,json数据对象
     */
    public String getConfigParam() {
        return configParam;
    }

    /**
     * 设置配置参数,json数据对象
     *
     * @param configParam 配置参数,json数据对象
     */
    public void setConfigParam(String configParam) {
        this.configParam = configParam == null ? null : configParam.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append(", className=").append(className);
        sb.append(", logo=").append(logo);
        sb.append(", status=").append(status);
        sb.append(", order=").append(order);
        sb.append(", poundage=").append(poundage);
        sb.append(", poundageType=").append(poundageType);
        sb.append(", clientType=").append(clientType);
        sb.append(", description=").append(description);
        sb.append(", note=").append(note);
        sb.append(", configParam=").append(configParam);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}