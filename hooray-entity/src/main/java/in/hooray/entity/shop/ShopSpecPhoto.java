package in.hooray.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_spec_photo")
public class ShopSpecPhoto implements Serializable {
    @Id
    private Integer id;

    /**
     * 图片地址
     */
    private String address;

    /**
     * 图片名称
     */
    private String name;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取图片地址
     *
     * @return address - 图片地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置图片地址
     *
     * @param address 图片地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取图片名称
     *
     * @return name - 图片名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置图片名称
     *
     * @param name 图片名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", address=").append(address);
        sb.append(", name=").append(name);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}