package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "shop_order_goods")
public class ShopOrderGoods implements Serializable {
    @Id
    private Integer id;

    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 商品图片
     */
    private String img;

    /**
     * 货品ID
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 商品价格
     */
    @Column(name = "goods_price")
    private BigDecimal goodsPrice;

    /**
     * 实付金额
     */
    @Column(name = "real_price")
    private BigDecimal realPrice;

    /**
     * 商品数量
     */
    @Column(name = "goods_nums")
    private Integer goodsNums;

    /**
     * 重量
     */
    @Column(name = "goods_weight")
    private BigDecimal goodsWeight;

    /**
     * 是否已发货 0:未发货;1:已发货;2:已经退货
     */
    @Column(name = "is_send")
    private Boolean isSend;

    /**
     * 是否给商家结算货款 0:未结算;1:已结算
     */
    @Column(name = "is_checkout")
    private Boolean isCheckout;

    /**
     * 配送单ID
     */
    @Column(name = "delivery_id")
    private Integer deliveryId;

    /**
     * 商品和货品名称name和规格value串json数据格式
     */
    @Column(name = "goods_array")
    private String goodsArray;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单ID
     *
     * @return order_id - 订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单ID
     *
     * @param orderId 订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取商品ID
     *
     * @return goods_id - 商品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置商品ID
     *
     * @param goodsId 商品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取商品图片
     *
     * @return img - 商品图片
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置商品图片
     *
     * @param img 商品图片
     */
    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    /**
     * 获取货品ID
     *
     * @return product_id - 货品ID
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * 设置货品ID
     *
     * @param productId 货品ID
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * 获取商品价格
     *
     * @return goods_price - 商品价格
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 设置商品价格
     *
     * @param goodsPrice 商品价格
     */
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 获取实付金额
     *
     * @return real_price - 实付金额
     */
    public BigDecimal getRealPrice() {
        return realPrice;
    }

    /**
     * 设置实付金额
     *
     * @param realPrice 实付金额
     */
    public void setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
    }

    /**
     * 获取商品数量
     *
     * @return goods_nums - 商品数量
     */
    public Integer getGoodsNums() {
        return goodsNums;
    }

    /**
     * 设置商品数量
     *
     * @param goodsNums 商品数量
     */
    public void setGoodsNums(Integer goodsNums) {
        this.goodsNums = goodsNums;
    }

    /**
     * 获取重量
     *
     * @return goods_weight - 重量
     */
    public BigDecimal getGoodsWeight() {
        return goodsWeight;
    }

    /**
     * 设置重量
     *
     * @param goodsWeight 重量
     */
    public void setGoodsWeight(BigDecimal goodsWeight) {
        this.goodsWeight = goodsWeight;
    }

    /**
     * 获取是否已发货 0:未发货;1:已发货;2:已经退货
     *
     * @return is_send - 是否已发货 0:未发货;1:已发货;2:已经退货
     */
    public Boolean getIsSend() {
        return isSend;
    }

    /**
     * 设置是否已发货 0:未发货;1:已发货;2:已经退货
     *
     * @param isSend 是否已发货 0:未发货;1:已发货;2:已经退货
     */
    public void setIsSend(Boolean isSend) {
        this.isSend = isSend;
    }

    /**
     * 获取是否给商家结算货款 0:未结算;1:已结算
     *
     * @return is_checkout - 是否给商家结算货款 0:未结算;1:已结算
     */
    public Boolean getIsCheckout() {
        return isCheckout;
    }

    /**
     * 设置是否给商家结算货款 0:未结算;1:已结算
     *
     * @param isCheckout 是否给商家结算货款 0:未结算;1:已结算
     */
    public void setIsCheckout(Boolean isCheckout) {
        this.isCheckout = isCheckout;
    }

    /**
     * 获取配送单ID
     *
     * @return delivery_id - 配送单ID
     */
    public Integer getDeliveryId() {
        return deliveryId;
    }

    /**
     * 设置配送单ID
     *
     * @param deliveryId 配送单ID
     */
    public void setDeliveryId(Integer deliveryId) {
        this.deliveryId = deliveryId;
    }

    /**
     * 获取商品和货品名称name和规格value串json数据格式
     *
     * @return goods_array - 商品和货品名称name和规格value串json数据格式
     */
    public String getGoodsArray() {
        return goodsArray;
    }

    /**
     * 设置商品和货品名称name和规格value串json数据格式
     *
     * @param goodsArray 商品和货品名称name和规格value串json数据格式
     */
    public void setGoodsArray(String goodsArray) {
        this.goodsArray = goodsArray == null ? null : goodsArray.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", img=").append(img);
        sb.append(", productId=").append(productId);
        sb.append(", goodsPrice=").append(goodsPrice);
        sb.append(", realPrice=").append(realPrice);
        sb.append(", goodsNums=").append(goodsNums);
        sb.append(", goodsWeight=").append(goodsWeight);
        sb.append(", isSend=").append(isSend);
        sb.append(", isCheckout=").append(isCheckout);
        sb.append(", deliveryId=").append(deliveryId);
        sb.append(", goodsArray=").append(goodsArray);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}