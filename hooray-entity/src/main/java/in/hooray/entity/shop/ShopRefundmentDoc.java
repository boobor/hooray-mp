package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_refundment_doc")
public class ShopRefundmentDoc implements Serializable {
    @Id
    private Integer id;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 退款金额
     */
    private BigDecimal amount;

    /**
     * 时间
     */
    private Date time;

    /**
     * 管理员id
     */
    @Column(name = "admin_id")
    private Integer adminId;

    /**
     * 退款状态，0:申请退款 1:退款失败 2:退款成功
     */
    @Column(name = "pay_status")
    private Boolean payStatus;

    /**
     * 处理时间
     */
    @Column(name = "dispose_time")
    private Date disposeTime;

    /**
     * 0:未删除 1:删除
     */
    @Column(name = "if_del")
    private Boolean ifDel;

    /**
     * 要退款的商品
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 要退款的货品
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 商家ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;

    /**
     * 申请退款原因
     */
    private String content;

    /**
     * 处理意见
     */
    @Column(name = "dispose_idea")
    private String disposeIdea;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取退款金额
     *
     * @return amount - 退款金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置退款金额
     *
     * @param amount 退款金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取时间
     *
     * @return time - 时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取管理员id
     *
     * @return admin_id - 管理员id
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * 设置管理员id
     *
     * @param adminId 管理员id
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * 获取退款状态，0:申请退款 1:退款失败 2:退款成功
     *
     * @return pay_status - 退款状态，0:申请退款 1:退款失败 2:退款成功
     */
    public Boolean getPayStatus() {
        return payStatus;
    }

    /**
     * 设置退款状态，0:申请退款 1:退款失败 2:退款成功
     *
     * @param payStatus 退款状态，0:申请退款 1:退款失败 2:退款成功
     */
    public void setPayStatus(Boolean payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * 获取处理时间
     *
     * @return dispose_time - 处理时间
     */
    public Date getDisposeTime() {
        return disposeTime;
    }

    /**
     * 设置处理时间
     *
     * @param disposeTime 处理时间
     */
    public void setDisposeTime(Date disposeTime) {
        this.disposeTime = disposeTime;
    }

    /**
     * 获取0:未删除 1:删除
     *
     * @return if_del - 0:未删除 1:删除
     */
    public Boolean getIfDel() {
        return ifDel;
    }

    /**
     * 设置0:未删除 1:删除
     *
     * @param ifDel 0:未删除 1:删除
     */
    public void setIfDel(Boolean ifDel) {
        this.ifDel = ifDel;
    }

    /**
     * 获取要退款的商品
     *
     * @return goods_id - 要退款的商品
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置要退款的商品
     *
     * @param goodsId 要退款的商品
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取要退款的货品
     *
     * @return product_id - 要退款的货品
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * 设置要退款的货品
     *
     * @param productId 要退款的货品
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * 获取商家ID
     *
     * @return seller_id - 商家ID
     */
    public Integer getSellerId() {
        return sellerId;
    }

    /**
     * 设置商家ID
     *
     * @param sellerId 商家ID
     */
    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * 获取申请退款原因
     *
     * @return content - 申请退款原因
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置申请退款原因
     *
     * @param content 申请退款原因
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取处理意见
     *
     * @return dispose_idea - 处理意见
     */
    public String getDisposeIdea() {
        return disposeIdea;
    }

    /**
     * 设置处理意见
     *
     * @param disposeIdea 处理意见
     */
    public void setDisposeIdea(String disposeIdea) {
        this.disposeIdea = disposeIdea == null ? null : disposeIdea.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", orderId=").append(orderId);
        sb.append(", userId=").append(userId);
        sb.append(", amount=").append(amount);
        sb.append(", time=").append(time);
        sb.append(", adminId=").append(adminId);
        sb.append(", payStatus=").append(payStatus);
        sb.append(", disposeTime=").append(disposeTime);
        sb.append(", ifDel=").append(ifDel);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", productId=").append(productId);
        sb.append(", sellerId=").append(sellerId);
        sb.append(", content=").append(content);
        sb.append(", disposeIdea=").append(disposeIdea);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}