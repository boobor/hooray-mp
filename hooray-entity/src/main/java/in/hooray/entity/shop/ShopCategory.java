package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_category")
public class ShopCategory implements Serializable {
    /**
     * 分类ID
     */
    @Id
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 父分类ID
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 首页是否显示 1显示 0 不显示
     */
    private Boolean visibility;

    /**
     * SEO关键词和检索关键词
     */
    private String keywords;

    /**
     * SEO描述
     */
    private String descript;

    /**
     * SEO标题title
     */
    private String title;

    private static final long serialVersionUID = 1L;

    /**
     * 获取分类ID
     *
     * @return id - 分类ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置分类ID
     *
     * @param id 分类ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取分类名称
     *
     * @return name - 分类名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置分类名称
     *
     * @param name 分类名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取父分类ID
     *
     * @return parent_id - 父分类ID
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父分类ID
     *
     * @param parentId 父分类ID
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    /**
     * 获取首页是否显示 1显示 0 不显示
     *
     * @return visibility - 首页是否显示 1显示 0 不显示
     */
    public Boolean getVisibility() {
        return visibility;
    }

    /**
     * 设置首页是否显示 1显示 0 不显示
     *
     * @param visibility 首页是否显示 1显示 0 不显示
     */
    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    /**
     * 获取SEO关键词和检索关键词
     *
     * @return keywords - SEO关键词和检索关键词
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * 设置SEO关键词和检索关键词
     *
     * @param keywords SEO关键词和检索关键词
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    /**
     * 获取SEO描述
     *
     * @return descript - SEO描述
     */
    public String getDescript() {
        return descript;
    }

    /**
     * 设置SEO描述
     *
     * @param descript SEO描述
     */
    public void setDescript(String descript) {
        this.descript = descript == null ? null : descript.trim();
    }

    /**
     * 获取SEO标题title
     *
     * @return title - SEO标题title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置SEO标题title
     *
     * @param title SEO标题title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", parentId=").append(parentId);
        sb.append(", sort=").append(sort);
        sb.append(", visibility=").append(visibility);
        sb.append(", keywords=").append(keywords);
        sb.append(", descript=").append(descript);
        sb.append(", title=").append(title);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}