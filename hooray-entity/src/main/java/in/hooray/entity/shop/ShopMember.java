package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_member")
public class ShopMember implements Serializable {
    /**
     * 用户ID
     */
    @Id
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 真实姓名
     */
    @Column(name = "true_name")
    private String trueName;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 地区
     */
    private String area;

    /**
     * 联系地址
     */
    @Column(name = "contact_addr")
    private String contactAddr;

    /**
     * QQ
     */
    private String qq;

    /**
     * MSN
     */
    private String msn;

    /**
     * 性别1男2女
     */
    private Boolean sex;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 分组
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 经验值
     */
    private Integer exp;

    /**
     * 积分
     */
    private Integer point;

    /**
     * 注册日期时间
     */
    private Date time;

    /**
     * 邮政编码
     */
    private String zip;

    /**
     * 用户状态 1正常状态 2 删除至回收站 3锁定
     */
    private Boolean status;

    /**
     * 用户余额
     */
    private BigDecimal balance;

    /**
     * 最后一次登录时间
     */
    @Column(name = "last_login")
    private Date lastLogin;

    /**
     * 用户习惯方式,配送和支付方式等信息
     */
    private String custom;

    /**
     * 消息ID
     */
    @Column(name = "message_ids")
    private String messageIds;

    /**
     * 用户拥有的工具
     */
    private String prop;

    private static final long serialVersionUID = 1L;

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取真实姓名
     *
     * @return true_name - 真实姓名
     */
    public String getTrueName() {
        return trueName;
    }

    /**
     * 设置真实姓名
     *
     * @param trueName 真实姓名
     */
    public void setTrueName(String trueName) {
        this.trueName = trueName == null ? null : trueName.trim();
    }

    /**
     * 获取联系电话
     *
     * @return telephone - 联系电话
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * 设置联系电话
     *
     * @param telephone 联系电话
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    /**
     * 获取手机
     *
     * @return mobile - 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机
     *
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取地区
     *
     * @return area - 地区
     */
    public String getArea() {
        return area;
    }

    /**
     * 设置地区
     *
     * @param area 地区
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    /**
     * 获取联系地址
     *
     * @return contact_addr - 联系地址
     */
    public String getContactAddr() {
        return contactAddr;
    }

    /**
     * 设置联系地址
     *
     * @param contactAddr 联系地址
     */
    public void setContactAddr(String contactAddr) {
        this.contactAddr = contactAddr == null ? null : contactAddr.trim();
    }

    /**
     * 获取QQ
     *
     * @return qq - QQ
     */
    public String getQq() {
        return qq;
    }

    /**
     * 设置QQ
     *
     * @param qq QQ
     */
    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    /**
     * 获取MSN
     *
     * @return msn - MSN
     */
    public String getMsn() {
        return msn;
    }

    /**
     * 设置MSN
     *
     * @param msn MSN
     */
    public void setMsn(String msn) {
        this.msn = msn == null ? null : msn.trim();
    }

    /**
     * 获取性别1男2女
     *
     * @return sex - 性别1男2女
     */
    public Boolean getSex() {
        return sex;
    }

    /**
     * 设置性别1男2女
     *
     * @param sex 性别1男2女
     */
    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    /**
     * 获取生日
     *
     * @return birthday - 生日
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * 设置生日
     *
     * @param birthday 生日
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * 获取分组
     *
     * @return group_id - 分组
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * 设置分组
     *
     * @param groupId 分组
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取经验值
     *
     * @return exp - 经验值
     */
    public Integer getExp() {
        return exp;
    }

    /**
     * 设置经验值
     *
     * @param exp 经验值
     */
    public void setExp(Integer exp) {
        this.exp = exp;
    }

    /**
     * 获取积分
     *
     * @return point - 积分
     */
    public Integer getPoint() {
        return point;
    }

    /**
     * 设置积分
     *
     * @param point 积分
     */
    public void setPoint(Integer point) {
        this.point = point;
    }

    /**
     * 获取注册日期时间
     *
     * @return time - 注册日期时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置注册日期时间
     *
     * @param time 注册日期时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取邮政编码
     *
     * @return zip - 邮政编码
     */
    public String getZip() {
        return zip;
    }

    /**
     * 设置邮政编码
     *
     * @param zip 邮政编码
     */
    public void setZip(String zip) {
        this.zip = zip == null ? null : zip.trim();
    }

    /**
     * 获取用户状态 1正常状态 2 删除至回收站 3锁定
     *
     * @return status - 用户状态 1正常状态 2 删除至回收站 3锁定
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置用户状态 1正常状态 2 删除至回收站 3锁定
     *
     * @param status 用户状态 1正常状态 2 删除至回收站 3锁定
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取用户余额
     *
     * @return balance - 用户余额
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * 设置用户余额
     *
     * @param balance 用户余额
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * 获取最后一次登录时间
     *
     * @return last_login - 最后一次登录时间
     */
    public Date getLastLogin() {
        return lastLogin;
    }

    /**
     * 设置最后一次登录时间
     *
     * @param lastLogin 最后一次登录时间
     */
    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * 获取用户习惯方式,配送和支付方式等信息
     *
     * @return custom - 用户习惯方式,配送和支付方式等信息
     */
    public String getCustom() {
        return custom;
    }

    /**
     * 设置用户习惯方式,配送和支付方式等信息
     *
     * @param custom 用户习惯方式,配送和支付方式等信息
     */
    public void setCustom(String custom) {
        this.custom = custom == null ? null : custom.trim();
    }

    /**
     * 获取消息ID
     *
     * @return message_ids - 消息ID
     */
    public String getMessageIds() {
        return messageIds;
    }

    /**
     * 设置消息ID
     *
     * @param messageIds 消息ID
     */
    public void setMessageIds(String messageIds) {
        this.messageIds = messageIds == null ? null : messageIds.trim();
    }

    /**
     * 获取用户拥有的工具
     *
     * @return prop - 用户拥有的工具
     */
    public String getProp() {
        return prop;
    }

    /**
     * 设置用户拥有的工具
     *
     * @param prop 用户拥有的工具
     */
    public void setProp(String prop) {
        this.prop = prop == null ? null : prop.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", trueName=").append(trueName);
        sb.append(", telephone=").append(telephone);
        sb.append(", mobile=").append(mobile);
        sb.append(", area=").append(area);
        sb.append(", contactAddr=").append(contactAddr);
        sb.append(", qq=").append(qq);
        sb.append(", msn=").append(msn);
        sb.append(", sex=").append(sex);
        sb.append(", birthday=").append(birthday);
        sb.append(", groupId=").append(groupId);
        sb.append(", exp=").append(exp);
        sb.append(", point=").append(point);
        sb.append(", time=").append(time);
        sb.append(", zip=").append(zip);
        sb.append(", status=").append(status);
        sb.append(", balance=").append(balance);
        sb.append(", lastLogin=").append(lastLogin);
        sb.append(", custom=").append(custom);
        sb.append(", messageIds=").append(messageIds);
        sb.append(", prop=").append(prop);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}