package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_brand")
public class ShopBrand implements Serializable {
    /**
     * 品牌ID
     */
    @Id
    private Integer id;

    /**
     * 品牌名称
     */
    private String name;

    /**
     * logo地址
     */
    private String logo;

    /**
     * 网址
     */
    private String url;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 品牌分类,逗号分割id
     */
    @Column(name = "category_ids")
    private String categoryIds;

    /**
     * 描述
     */
    private String description;

    private static final long serialVersionUID = 1L;

    /**
     * 获取品牌ID
     *
     * @return id - 品牌ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置品牌ID
     *
     * @param id 品牌ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取品牌名称
     *
     * @return name - 品牌名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置品牌名称
     *
     * @param name 品牌名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取logo地址
     *
     * @return logo - logo地址
     */
    public String getLogo() {
        return logo;
    }

    /**
     * 设置logo地址
     *
     * @param logo logo地址
     */
    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    /**
     * 获取网址
     *
     * @return url - 网址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置网址
     *
     * @param url 网址
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    /**
     * 获取品牌分类,逗号分割id
     *
     * @return category_ids - 品牌分类,逗号分割id
     */
    public String getCategoryIds() {
        return categoryIds;
    }

    /**
     * 设置品牌分类,逗号分割id
     *
     * @param categoryIds 品牌分类,逗号分割id
     */
    public void setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds == null ? null : categoryIds.trim();
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", logo=").append(logo);
        sb.append(", url=").append(url);
        sb.append(", sort=").append(sort);
        sb.append(", categoryIds=").append(categoryIds);
        sb.append(", description=").append(description);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}