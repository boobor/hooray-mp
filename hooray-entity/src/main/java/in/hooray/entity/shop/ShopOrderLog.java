package in.hooray.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_order_log")
public class ShopOrderLog implements Serializable {
    @Id
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 操作人：顾客或admin或seller
     */
    private String user;

    /**
     * 动作
     */
    private String action;

    /**
     * 添加时间
     */
    private Date addtime;

    /**
     * 操作的结果
     */
    private String result;

    /**
     * 备注
     */
    private String note;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取操作人：顾客或admin或seller
     *
     * @return user - 操作人：顾客或admin或seller
     */
    public String getUser() {
        return user;
    }

    /**
     * 设置操作人：顾客或admin或seller
     *
     * @param user 操作人：顾客或admin或seller
     */
    public void setUser(String user) {
        this.user = user == null ? null : user.trim();
    }

    /**
     * 获取动作
     *
     * @return action - 动作
     */
    public String getAction() {
        return action;
    }

    /**
     * 设置动作
     *
     * @param action 动作
     */
    public void setAction(String action) {
        this.action = action == null ? null : action.trim();
    }

    /**
     * 获取添加时间
     *
     * @return addtime - 添加时间
     */
    public Date getAddtime() {
        return addtime;
    }

    /**
     * 设置添加时间
     *
     * @param addtime 添加时间
     */
    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    /**
     * 获取操作的结果
     *
     * @return result - 操作的结果
     */
    public String getResult() {
        return result;
    }

    /**
     * 设置操作的结果
     *
     * @param result 操作的结果
     */
    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", user=").append(user);
        sb.append(", action=").append(action);
        sb.append(", addtime=").append(addtime);
        sb.append(", result=").append(result);
        sb.append(", note=").append(note);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}