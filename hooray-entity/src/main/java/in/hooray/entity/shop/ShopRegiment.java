package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_regiment")
public class ShopRegiment implements Serializable {
    @Id
    private Integer id;

    /**
     * 团购标题
     */
    private String title;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 库存量
     */
    @Column(name = "store_nums")
    private Integer storeNums;

    /**
     * 已销售量
     */
    @Column(name = "sum_count")
    private Integer sumCount;

    /**
     * 每人限制最少购买数量
     */
    @Column(name = "limit_min_count")
    private Integer limitMinCount;

    /**
     * 每人限制最多购买数量
     */
    @Column(name = "limit_max_count")
    private Integer limitMaxCount;

    /**
     * 介绍
     */
    private String intro;

    /**
     * 是否关闭
     */
    @Column(name = "is_close")
    private Boolean isClose;

    /**
     * 团购价格
     */
    @Column(name = "regiment_price")
    private BigDecimal regimentPrice;

    /**
     * 原来价格
     */
    @Column(name = "sell_price")
    private BigDecimal sellPrice;

    /**
     * 关联商品id
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 商品图片
     */
    private String img;

    /**
     * 排序
     */
    private Short sort;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取团购标题
     *
     * @return title - 团购标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置团购标题
     *
     * @param title 团购标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取开始时间
     *
     * @return start_time - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取库存量
     *
     * @return store_nums - 库存量
     */
    public Integer getStoreNums() {
        return storeNums;
    }

    /**
     * 设置库存量
     *
     * @param storeNums 库存量
     */
    public void setStoreNums(Integer storeNums) {
        this.storeNums = storeNums;
    }

    /**
     * 获取已销售量
     *
     * @return sum_count - 已销售量
     */
    public Integer getSumCount() {
        return sumCount;
    }

    /**
     * 设置已销售量
     *
     * @param sumCount 已销售量
     */
    public void setSumCount(Integer sumCount) {
        this.sumCount = sumCount;
    }

    /**
     * 获取每人限制最少购买数量
     *
     * @return limit_min_count - 每人限制最少购买数量
     */
    public Integer getLimitMinCount() {
        return limitMinCount;
    }

    /**
     * 设置每人限制最少购买数量
     *
     * @param limitMinCount 每人限制最少购买数量
     */
    public void setLimitMinCount(Integer limitMinCount) {
        this.limitMinCount = limitMinCount;
    }

    /**
     * 获取每人限制最多购买数量
     *
     * @return limit_max_count - 每人限制最多购买数量
     */
    public Integer getLimitMaxCount() {
        return limitMaxCount;
    }

    /**
     * 设置每人限制最多购买数量
     *
     * @param limitMaxCount 每人限制最多购买数量
     */
    public void setLimitMaxCount(Integer limitMaxCount) {
        this.limitMaxCount = limitMaxCount;
    }

    /**
     * 获取介绍
     *
     * @return intro - 介绍
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 设置介绍
     *
     * @param intro 介绍
     */
    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    /**
     * 获取是否关闭
     *
     * @return is_close - 是否关闭
     */
    public Boolean getIsClose() {
        return isClose;
    }

    /**
     * 设置是否关闭
     *
     * @param isClose 是否关闭
     */
    public void setIsClose(Boolean isClose) {
        this.isClose = isClose;
    }

    /**
     * 获取团购价格
     *
     * @return regiment_price - 团购价格
     */
    public BigDecimal getRegimentPrice() {
        return regimentPrice;
    }

    /**
     * 设置团购价格
     *
     * @param regimentPrice 团购价格
     */
    public void setRegimentPrice(BigDecimal regimentPrice) {
        this.regimentPrice = regimentPrice;
    }

    /**
     * 获取原来价格
     *
     * @return sell_price - 原来价格
     */
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    /**
     * 设置原来价格
     *
     * @param sellPrice 原来价格
     */
    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 获取关联商品id
     *
     * @return goods_id - 关联商品id
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置关联商品id
     *
     * @param goodsId 关联商品id
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取商品图片
     *
     * @return img - 商品图片
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置商品图片
     *
     * @param img 商品图片
     */
    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", storeNums=").append(storeNums);
        sb.append(", sumCount=").append(sumCount);
        sb.append(", limitMinCount=").append(limitMinCount);
        sb.append(", limitMaxCount=").append(limitMaxCount);
        sb.append(", intro=").append(intro);
        sb.append(", isClose=").append(isClose);
        sb.append(", regimentPrice=").append(regimentPrice);
        sb.append(", sellPrice=").append(sellPrice);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", img=").append(img);
        sb.append(", sort=").append(sort);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}