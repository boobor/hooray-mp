package in.hooray.entity.mp;


/**
 * 代表公众号实体
 * @author daqing
 *
 */
public class MpAccount {

	private Long id;
	
	private String hash;
	private String appid;
	private String token;
	private String aeskey;
	private String description;
	private String name;
	private Integer status;
	private String thumb;
	
	public MpAccount() {}
	
	public MpAccount(Long id, String appid, String token, String hash) {
		this.id = id;
		this.appid = appid;
		this.token = token;
		this.hash = hash;
	}
	
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAeskey() {
		return aeskey;
	}
	public void setAeskey(String aeskey) {
		this.aeskey = aeskey;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
}
