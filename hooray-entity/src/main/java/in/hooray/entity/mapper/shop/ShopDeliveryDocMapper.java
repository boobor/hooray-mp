package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopDeliveryDoc;
import in.hooray.entity.shop.ShopDeliveryDocCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopDeliveryDocMapper extends MyMapper<ShopDeliveryDoc> {
    int countByExample(ShopDeliveryDocCriteria example);

    int deleteByExample(ShopDeliveryDocCriteria example);

    List<ShopDeliveryDoc> selectByExampleWithBLOBsWithRowbounds(ShopDeliveryDocCriteria example, RowBounds rowBounds);

    List<ShopDeliveryDoc> selectByExampleWithBLOBs(ShopDeliveryDocCriteria example);

    List<ShopDeliveryDoc> selectByExampleWithRowbounds(ShopDeliveryDocCriteria example, RowBounds rowBounds);

    List<ShopDeliveryDoc> selectByExample(ShopDeliveryDocCriteria example);

    int updateByExampleSelective(@Param("record") ShopDeliveryDoc record, @Param("example") ShopDeliveryDocCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopDeliveryDoc record, @Param("example") ShopDeliveryDocCriteria example);

    int updateByExample(@Param("record") ShopDeliveryDoc record, @Param("example") ShopDeliveryDocCriteria example);
}