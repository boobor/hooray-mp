package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopAddress;
import in.hooray.entity.shop.ShopAddressCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopAddressMapper extends MyMapper<ShopAddress> {
    int countByExample(ShopAddressCriteria example);

    int deleteByExample(ShopAddressCriteria example);

    List<ShopAddress> selectByExampleWithRowbounds(ShopAddressCriteria example, RowBounds rowBounds);

    List<ShopAddress> selectByExample(ShopAddressCriteria example);

    int updateByExampleSelective(@Param("record") ShopAddress record, @Param("example") ShopAddressCriteria example);

    int updateByExample(@Param("record") ShopAddress record, @Param("example") ShopAddressCriteria example);
}