package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopCollectionDoc;
import in.hooray.entity.shop.ShopCollectionDocCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopCollectionDocMapper extends MyMapper<ShopCollectionDoc> {
    int countByExample(ShopCollectionDocCriteria example);

    int deleteByExample(ShopCollectionDocCriteria example);

    List<ShopCollectionDoc> selectByExampleWithBLOBsWithRowbounds(ShopCollectionDocCriteria example, RowBounds rowBounds);

    List<ShopCollectionDoc> selectByExampleWithBLOBs(ShopCollectionDocCriteria example);

    List<ShopCollectionDoc> selectByExampleWithRowbounds(ShopCollectionDocCriteria example, RowBounds rowBounds);

    List<ShopCollectionDoc> selectByExample(ShopCollectionDocCriteria example);

    int updateByExampleSelective(@Param("record") ShopCollectionDoc record, @Param("example") ShopCollectionDocCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopCollectionDoc record, @Param("example") ShopCollectionDocCriteria example);

    int updateByExample(@Param("record") ShopCollectionDoc record, @Param("example") ShopCollectionDocCriteria example);
}