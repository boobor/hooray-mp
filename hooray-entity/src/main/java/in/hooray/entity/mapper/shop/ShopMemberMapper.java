package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopMember;
import in.hooray.entity.shop.ShopMemberCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopMemberMapper extends MyMapper<ShopMember> {
    int countByExample(ShopMemberCriteria example);

    int deleteByExample(ShopMemberCriteria example);

    List<ShopMember> selectByExampleWithBLOBsWithRowbounds(ShopMemberCriteria example, RowBounds rowBounds);

    List<ShopMember> selectByExampleWithBLOBs(ShopMemberCriteria example);

    List<ShopMember> selectByExampleWithRowbounds(ShopMemberCriteria example, RowBounds rowBounds);

    List<ShopMember> selectByExample(ShopMemberCriteria example);

    int updateByExampleSelective(@Param("record") ShopMember record, @Param("example") ShopMemberCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopMember record, @Param("example") ShopMemberCriteria example);

    int updateByExample(@Param("record") ShopMember record, @Param("example") ShopMemberCriteria example);
}