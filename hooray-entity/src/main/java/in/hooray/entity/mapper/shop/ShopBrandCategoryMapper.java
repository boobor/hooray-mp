package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopBrandCategory;
import in.hooray.entity.shop.ShopBrandCategoryCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopBrandCategoryMapper extends MyMapper<ShopBrandCategory> {
    int countByExample(ShopBrandCategoryCriteria example);

    int deleteByExample(ShopBrandCategoryCriteria example);

    List<ShopBrandCategory> selectByExampleWithRowbounds(ShopBrandCategoryCriteria example, RowBounds rowBounds);

    List<ShopBrandCategory> selectByExample(ShopBrandCategoryCriteria example);

    int updateByExampleSelective(@Param("record") ShopBrandCategory record, @Param("example") ShopBrandCategoryCriteria example);

    int updateByExample(@Param("record") ShopBrandCategory record, @Param("example") ShopBrandCategoryCriteria example);
}