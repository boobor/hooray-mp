package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopPointLog;
import in.hooray.entity.shop.ShopPointLogCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopPointLogMapper extends MyMapper<ShopPointLog> {
    int countByExample(ShopPointLogCriteria example);

    int deleteByExample(ShopPointLogCriteria example);

    List<ShopPointLog> selectByExampleWithRowbounds(ShopPointLogCriteria example, RowBounds rowBounds);

    List<ShopPointLog> selectByExample(ShopPointLogCriteria example);

    int updateByExampleSelective(@Param("record") ShopPointLog record, @Param("example") ShopPointLogCriteria example);

    int updateByExample(@Param("record") ShopPointLog record, @Param("example") ShopPointLogCriteria example);
}