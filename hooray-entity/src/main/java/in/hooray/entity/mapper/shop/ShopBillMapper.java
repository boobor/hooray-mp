package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopBill;
import in.hooray.entity.shop.ShopBillCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopBillMapper extends MyMapper<ShopBill> {
    int countByExample(ShopBillCriteria example);

    int deleteByExample(ShopBillCriteria example);

    List<ShopBill> selectByExampleWithBLOBsWithRowbounds(ShopBillCriteria example, RowBounds rowBounds);

    List<ShopBill> selectByExampleWithBLOBs(ShopBillCriteria example);

    List<ShopBill> selectByExampleWithRowbounds(ShopBillCriteria example, RowBounds rowBounds);

    List<ShopBill> selectByExample(ShopBillCriteria example);

    int updateByExampleSelective(@Param("record") ShopBill record, @Param("example") ShopBillCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopBill record, @Param("example") ShopBillCriteria example);

    int updateByExample(@Param("record") ShopBill record, @Param("example") ShopBillCriteria example);
}