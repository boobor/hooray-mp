package in.hooray.entity.mapper;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.mp.AccountWechat;
import in.hooray.entity.mp.AccountWechatCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AccountWechatMapper extends MyMapper<AccountWechat> {
    int countByExample(AccountWechatCriteria example);

    int deleteByExample(AccountWechatCriteria example);

    List<AccountWechat> selectByExampleWithRowbounds(AccountWechatCriteria example, RowBounds rowBounds);

    List<AccountWechat> selectByExample(AccountWechatCriteria example);

    int updateByExampleSelective(@Param("record") AccountWechat record, @Param("example") AccountWechatCriteria example);

    int updateByExample(@Param("record") AccountWechat record, @Param("example") AccountWechatCriteria example);
}