package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopOrder;
import in.hooray.entity.shop.ShopOrderCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopOrderMapper extends MyMapper<ShopOrder> {
    int countByExample(ShopOrderCriteria example);

    int deleteByExample(ShopOrderCriteria example);

    List<ShopOrder> selectByExampleWithBLOBsWithRowbounds(ShopOrderCriteria example, RowBounds rowBounds);

    List<ShopOrder> selectByExampleWithBLOBs(ShopOrderCriteria example);

    List<ShopOrder> selectByExampleWithRowbounds(ShopOrderCriteria example, RowBounds rowBounds);

    List<ShopOrder> selectByExample(ShopOrderCriteria example);

    int updateByExampleSelective(@Param("record") ShopOrder record, @Param("example") ShopOrderCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopOrder record, @Param("example") ShopOrderCriteria example);

    int updateByExample(@Param("record") ShopOrder record, @Param("example") ShopOrderCriteria example);
}