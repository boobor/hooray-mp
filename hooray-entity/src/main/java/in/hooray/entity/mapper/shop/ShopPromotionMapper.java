package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopPromotion;
import in.hooray.entity.shop.ShopPromotionCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopPromotionMapper extends MyMapper<ShopPromotion> {
    int countByExample(ShopPromotionCriteria example);

    int deleteByExample(ShopPromotionCriteria example);

    List<ShopPromotion> selectByExampleWithBLOBsWithRowbounds(ShopPromotionCriteria example, RowBounds rowBounds);

    List<ShopPromotion> selectByExampleWithBLOBs(ShopPromotionCriteria example);

    List<ShopPromotion> selectByExampleWithRowbounds(ShopPromotionCriteria example, RowBounds rowBounds);

    List<ShopPromotion> selectByExample(ShopPromotionCriteria example);

    int updateByExampleSelective(@Param("record") ShopPromotion record, @Param("example") ShopPromotionCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopPromotion record, @Param("example") ShopPromotionCriteria example);

    int updateByExample(@Param("record") ShopPromotion record, @Param("example") ShopPromotionCriteria example);
}