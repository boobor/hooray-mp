package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopSpec;
import in.hooray.entity.shop.ShopSpecCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopSpecMapper extends MyMapper<ShopSpec> {
    int countByExample(ShopSpecCriteria example);

    int deleteByExample(ShopSpecCriteria example);

    List<ShopSpec> selectByExampleWithBLOBsWithRowbounds(ShopSpecCriteria example, RowBounds rowBounds);

    List<ShopSpec> selectByExampleWithBLOBs(ShopSpecCriteria example);

    List<ShopSpec> selectByExampleWithRowbounds(ShopSpecCriteria example, RowBounds rowBounds);

    List<ShopSpec> selectByExample(ShopSpecCriteria example);

    int updateByExampleSelective(@Param("record") ShopSpec record, @Param("example") ShopSpecCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopSpec record, @Param("example") ShopSpecCriteria example);

    int updateByExample(@Param("record") ShopSpec record, @Param("example") ShopSpecCriteria example);
}