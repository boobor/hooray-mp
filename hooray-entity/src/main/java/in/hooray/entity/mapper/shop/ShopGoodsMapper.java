package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopGoods;
import in.hooray.entity.shop.ShopGoodsCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopGoodsMapper extends MyMapper<ShopGoods> {
    int countByExample(ShopGoodsCriteria example);

    int deleteByExample(ShopGoodsCriteria example);

    List<ShopGoods> selectByExampleWithBLOBsWithRowbounds(ShopGoodsCriteria example, RowBounds rowBounds);

    List<ShopGoods> selectByExampleWithBLOBs(ShopGoodsCriteria example);

    List<ShopGoods> selectByExampleWithRowbounds(ShopGoodsCriteria example, RowBounds rowBounds);

    List<ShopGoods> selectByExample(ShopGoodsCriteria example);

    int updateByExampleSelective(@Param("record") ShopGoods record, @Param("example") ShopGoodsCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopGoods record, @Param("example") ShopGoodsCriteria example);

    int updateByExample(@Param("record") ShopGoods record, @Param("example") ShopGoodsCriteria example);
}