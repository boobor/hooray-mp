package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopGoodsCar;
import in.hooray.entity.shop.ShopGoodsCarCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopGoodsCarMapper extends MyMapper<ShopGoodsCar> {
    int countByExample(ShopGoodsCarCriteria example);

    int deleteByExample(ShopGoodsCarCriteria example);

    List<ShopGoodsCar> selectByExampleWithBLOBsWithRowbounds(ShopGoodsCarCriteria example, RowBounds rowBounds);

    List<ShopGoodsCar> selectByExampleWithBLOBs(ShopGoodsCarCriteria example);

    List<ShopGoodsCar> selectByExampleWithRowbounds(ShopGoodsCarCriteria example, RowBounds rowBounds);

    List<ShopGoodsCar> selectByExample(ShopGoodsCarCriteria example);

    int updateByExampleSelective(@Param("record") ShopGoodsCar record, @Param("example") ShopGoodsCarCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopGoodsCar record, @Param("example") ShopGoodsCarCriteria example);

    int updateByExample(@Param("record") ShopGoodsCar record, @Param("example") ShopGoodsCarCriteria example);
}