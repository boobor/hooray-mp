package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopGoodsPhoto;
import in.hooray.entity.shop.ShopGoodsPhotoCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopGoodsPhotoMapper extends MyMapper<ShopGoodsPhoto> {
    int countByExample(ShopGoodsPhotoCriteria example);

    int deleteByExample(ShopGoodsPhotoCriteria example);

    List<ShopGoodsPhoto> selectByExampleWithRowbounds(ShopGoodsPhotoCriteria example, RowBounds rowBounds);

    List<ShopGoodsPhoto> selectByExample(ShopGoodsPhotoCriteria example);

    int updateByExampleSelective(@Param("record") ShopGoodsPhoto record, @Param("example") ShopGoodsPhotoCriteria example);

    int updateByExample(@Param("record") ShopGoodsPhoto record, @Param("example") ShopGoodsPhotoCriteria example);
}