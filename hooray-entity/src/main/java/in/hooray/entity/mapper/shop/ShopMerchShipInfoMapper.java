package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopMerchShipInfo;
import in.hooray.entity.shop.ShopMerchShipInfoCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopMerchShipInfoMapper extends MyMapper<ShopMerchShipInfo> {
    int countByExample(ShopMerchShipInfoCriteria example);

    int deleteByExample(ShopMerchShipInfoCriteria example);

    List<ShopMerchShipInfo> selectByExampleWithBLOBsWithRowbounds(ShopMerchShipInfoCriteria example, RowBounds rowBounds);

    List<ShopMerchShipInfo> selectByExampleWithBLOBs(ShopMerchShipInfoCriteria example);

    List<ShopMerchShipInfo> selectByExampleWithRowbounds(ShopMerchShipInfoCriteria example, RowBounds rowBounds);

    List<ShopMerchShipInfo> selectByExample(ShopMerchShipInfoCriteria example);

    int updateByExampleSelective(@Param("record") ShopMerchShipInfo record, @Param("example") ShopMerchShipInfoCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopMerchShipInfo record, @Param("example") ShopMerchShipInfoCriteria example);

    int updateByExample(@Param("record") ShopMerchShipInfo record, @Param("example") ShopMerchShipInfoCriteria example);
}