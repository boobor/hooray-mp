package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopPayment;
import in.hooray.entity.shop.ShopPaymentCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopPaymentMapper extends MyMapper<ShopPayment> {
    int countByExample(ShopPaymentCriteria example);

    int deleteByExample(ShopPaymentCriteria example);

    List<ShopPayment> selectByExampleWithBLOBsWithRowbounds(ShopPaymentCriteria example, RowBounds rowBounds);

    List<ShopPayment> selectByExampleWithBLOBs(ShopPaymentCriteria example);

    List<ShopPayment> selectByExampleWithRowbounds(ShopPaymentCriteria example, RowBounds rowBounds);

    List<ShopPayment> selectByExample(ShopPaymentCriteria example);

    int updateByExampleSelective(@Param("record") ShopPayment record, @Param("example") ShopPaymentCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopPayment record, @Param("example") ShopPaymentCriteria example);

    int updateByExample(@Param("record") ShopPayment record, @Param("example") ShopPaymentCriteria example);
}