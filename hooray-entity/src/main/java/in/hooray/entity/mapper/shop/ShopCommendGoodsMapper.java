package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopCommendGoods;
import in.hooray.entity.shop.ShopCommendGoodsCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopCommendGoodsMapper extends MyMapper<ShopCommendGoods> {
    int countByExample(ShopCommendGoodsCriteria example);

    int deleteByExample(ShopCommendGoodsCriteria example);

    List<ShopCommendGoods> selectByExampleWithRowbounds(ShopCommendGoodsCriteria example, RowBounds rowBounds);

    List<ShopCommendGoods> selectByExample(ShopCommendGoodsCriteria example);

    int updateByExampleSelective(@Param("record") ShopCommendGoods record, @Param("example") ShopCommendGoodsCriteria example);

    int updateByExample(@Param("record") ShopCommendGoods record, @Param("example") ShopCommendGoodsCriteria example);
}