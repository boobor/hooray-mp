package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopCategory;
import in.hooray.entity.shop.ShopCategoryCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopCategoryMapper extends MyMapper<ShopCategory> {
    int countByExample(ShopCategoryCriteria example);

    int deleteByExample(ShopCategoryCriteria example);

    List<ShopCategory> selectByExampleWithRowbounds(ShopCategoryCriteria example, RowBounds rowBounds);

    List<ShopCategory> selectByExample(ShopCategoryCriteria example);

    int updateByExampleSelective(@Param("record") ShopCategory record, @Param("example") ShopCategoryCriteria example);

    int updateByExample(@Param("record") ShopCategory record, @Param("example") ShopCategoryCriteria example);
}