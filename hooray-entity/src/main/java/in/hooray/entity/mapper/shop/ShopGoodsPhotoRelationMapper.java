package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopGoodsPhotoRelation;
import in.hooray.entity.shop.ShopGoodsPhotoRelationCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopGoodsPhotoRelationMapper extends MyMapper<ShopGoodsPhotoRelation> {
    int countByExample(ShopGoodsPhotoRelationCriteria example);

    int deleteByExample(ShopGoodsPhotoRelationCriteria example);

    List<ShopGoodsPhotoRelation> selectByExampleWithRowbounds(ShopGoodsPhotoRelationCriteria example, RowBounds rowBounds);

    List<ShopGoodsPhotoRelation> selectByExample(ShopGoodsPhotoRelationCriteria example);

    int updateByExampleSelective(@Param("record") ShopGoodsPhotoRelation record, @Param("example") ShopGoodsPhotoRelationCriteria example);

    int updateByExample(@Param("record") ShopGoodsPhotoRelation record, @Param("example") ShopGoodsPhotoRelationCriteria example);
}