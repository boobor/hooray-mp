package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopBrand;
import in.hooray.entity.shop.ShopBrandCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopBrandMapper extends MyMapper<ShopBrand> {
    int countByExample(ShopBrandCriteria example);

    int deleteByExample(ShopBrandCriteria example);

    List<ShopBrand> selectByExampleWithBLOBsWithRowbounds(ShopBrandCriteria example, RowBounds rowBounds);

    List<ShopBrand> selectByExampleWithBLOBs(ShopBrandCriteria example);

    List<ShopBrand> selectByExampleWithRowbounds(ShopBrandCriteria example, RowBounds rowBounds);

    List<ShopBrand> selectByExample(ShopBrandCriteria example);

    int updateByExampleSelective(@Param("record") ShopBrand record, @Param("example") ShopBrandCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopBrand record, @Param("example") ShopBrandCriteria example);

    int updateByExample(@Param("record") ShopBrand record, @Param("example") ShopBrandCriteria example);
}