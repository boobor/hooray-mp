package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopProducts;
import in.hooray.entity.shop.ShopProductsCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopProductsMapper extends MyMapper<ShopProducts> {
    int countByExample(ShopProductsCriteria example);

    int deleteByExample(ShopProductsCriteria example);

    List<ShopProducts> selectByExampleWithBLOBsWithRowbounds(ShopProductsCriteria example, RowBounds rowBounds);

    List<ShopProducts> selectByExampleWithBLOBs(ShopProductsCriteria example);

    List<ShopProducts> selectByExampleWithRowbounds(ShopProductsCriteria example, RowBounds rowBounds);

    List<ShopProducts> selectByExample(ShopProductsCriteria example);

    int updateByExampleSelective(@Param("record") ShopProducts record, @Param("example") ShopProductsCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopProducts record, @Param("example") ShopProductsCriteria example);

    int updateByExample(@Param("record") ShopProducts record, @Param("example") ShopProductsCriteria example);
}