package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopOrderLog;
import in.hooray.entity.shop.ShopOrderLogCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopOrderLogMapper extends MyMapper<ShopOrderLog> {
    int countByExample(ShopOrderLogCriteria example);

    int deleteByExample(ShopOrderLogCriteria example);

    List<ShopOrderLog> selectByExampleWithRowbounds(ShopOrderLogCriteria example, RowBounds rowBounds);

    List<ShopOrderLog> selectByExample(ShopOrderLogCriteria example);

    int updateByExampleSelective(@Param("record") ShopOrderLog record, @Param("example") ShopOrderLogCriteria example);

    int updateByExample(@Param("record") ShopOrderLog record, @Param("example") ShopOrderLogCriteria example);
}