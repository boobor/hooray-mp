package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopSeller;
import in.hooray.entity.shop.ShopSellerCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopSellerMapper extends MyMapper<ShopSeller> {
    int countByExample(ShopSellerCriteria example);

    int deleteByExample(ShopSellerCriteria example);

    List<ShopSeller> selectByExampleWithBLOBsWithRowbounds(ShopSellerCriteria example, RowBounds rowBounds);

    List<ShopSeller> selectByExampleWithBLOBs(ShopSellerCriteria example);

    List<ShopSeller> selectByExampleWithRowbounds(ShopSellerCriteria example, RowBounds rowBounds);

    List<ShopSeller> selectByExample(ShopSellerCriteria example);

    int updateByExampleSelective(@Param("record") ShopSeller record, @Param("example") ShopSellerCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopSeller record, @Param("example") ShopSellerCriteria example);

    int updateByExample(@Param("record") ShopSeller record, @Param("example") ShopSellerCriteria example);
}