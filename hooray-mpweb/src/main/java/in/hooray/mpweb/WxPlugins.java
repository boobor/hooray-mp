package in.hooray.mpweb;


import in.hooray.core.utils.ReflectUtils;
import in.hooray.entity.mp.WxPlugin;
import in.hooray.mpsdk.plugin.IPlugin;
import in.hooray.service.mp.WxPluginService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
public class WxPlugins {
	
	private Logger log = LoggerFactory.getLogger(WxPlugins.class);
	
	private WxPluginService wxPluginService;
	
	private List<IPlugin> plugins = Lists.newLinkedList();
	
	public WxPlugins() {
	}
	
	@Autowired
	public WxPlugins(@Qualifier("wxPluginService") WxPluginService wxPluginService) {
		log.info("开始初始化微信插件服务：" + wxPluginService);
		this.wxPluginService = wxPluginService;
		this.initWxPlugin();
	}
	
	
	private void initWxPlugin() {
		List<WxPlugin> plugins = wxPluginService.getWxPlugins();
		for(WxPlugin plugin : plugins) {
			if(plugin.getStatus()) {
				log.info("开始实例化微信插件服务：" + plugin.getName() + ", classpath=" + plugin.getClazzpath());
				ReflectUtils reflectWrapper = ReflectUtils.on(plugin.getClazzpath());
				plugins.add(reflectWrapper.get());
			}
		}
	}

	public List<IPlugin> getPlugins() {
		return this.plugins;
	}
	
}
