package in.hooray.core.config.mvc;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

// @Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
	
	/**
     * 视图设置
     */
    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    /**
     * 扩展已至此上传进度监控
     */
    @Bean
    public MultipartResolver multipartResolver() {
        // CommonsMultipartResolver resolver = new FileUploadMultipartResolver();
        // resolver.setMaxUploadSize(1000000000);
        return null;
    }


    /**
     * 配置Servlet的Filter类
     * @return
     */
    @Bean(name = "sitemeshFilter")
    public FilterRegistrationBean sitemeshFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean();
        // bean.setFilter(new SitemeshFilter());
        bean.addUrlPatterns("/*");
        bean.setOrder(1111);
        return bean;
    }

    /**
     * 配置Servlet，一个Servlet就配置一个注解Bean
     * @return
     */
    @Bean(name = "captchaServlet")
    public ServletRegistrationBean captchaServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        // bean.setServlet(new CaptchaServlet());
        // bean.addUrlMappings("/captcha");
        return bean;
    }
    
}
