package in.hooray.core.event;

public interface EventSubscriber {
	
	void execute(EventSource event);
	
}
