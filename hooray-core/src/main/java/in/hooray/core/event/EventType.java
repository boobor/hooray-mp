package in.hooray.core.event;

/**
 * @author daqing
 *
 */
public enum EventType {
	
	WX_MASS_IMAGE,					// 微信群发的图片消息
	WX_MASS_TEXT,					// 微信群发的文本
	WX_MASS_NEWS,					// 微信群发的图文信息
	WX_MASS_STATUS,					// 微信群发消息的成功与否状态
	WX_MASS,						// 用户触发群发的事件
	WX_TEMPLATESENDJOBFINISH,		// 模版消息发送任务完成后，微信服务器会将是否送达成功作为通知，发送到开发者
	WX_GROUP_SET,
	WX_ACCOUNT_GROUP_SET, 			// 给公众号添加默认分组
	
	WX_REDPACK_ACTIVITY_FIRST, 		// 首次关注的新用户送红包
	
	ADMIN_OPERATOR_LOG,				// 后台操作日志事件类型
	
	WX_MSG_STORE, 					// 微信聊天记录保存
	WX_MENU_ACCESS,					// 微信菜单访问
	
	WX_CUSTOM_NEWS_SEND,			// 微信48小时交互内的客服图文消息
	
	BBS_USER_SCORE_INR, 			// BBS 用户积分增加事件
	
	WX_STAT_SITE,  					// 微信微站统计
	WX_GET_FOLLOWS, 				// 获取某个公众号的关注用户事件
}
