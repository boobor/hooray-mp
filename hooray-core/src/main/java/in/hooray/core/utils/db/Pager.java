package in.hooray.core.utils.db;

import java.util.List;

public class Pager<T> {
	
	private int total = 0; //总数
	
	private int rows = 10; //大小
	
	private int page = 1; //页数
	
	private int totalPage = 0; //总页数
	
	private List<T> pageList = null; //分页结果集
	
	public int getTotalPage() {
		totalPage = (total+rows-1)/rows ;
		return totalPage;
	}

	public List<T> getPageList() {
		return pageList;
	}

	public void setPageList(List<T> pageList) {
		this.pageList = pageList;
	}
	
	public Pager() {
		
	}
	
	public Pager(int page){
		this.page = page;
	}
	
	public Pager(int page,int rows){
		this.page = page;
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}
