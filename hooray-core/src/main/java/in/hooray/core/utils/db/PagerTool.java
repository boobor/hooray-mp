package in.hooray.core.utils.db;


import in.hooray.core.mybatis.mapper.BaseMapper;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

/**
 * mybatis分页工具
 */
public class PagerTool {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Pager getPager(BaseMapper baseMapper,Object example,Pager pager){
		int pageSize = pager.getRows(); //页大小
		int pageIndex = pager.getPage(); //当前页数
		
		//限制页数大小
		if(pageSize<1 || pageSize>50)
			pager.setRows(10);
		
		if(pageIndex<1)
			pager.setPage(1);
		
		//查询数据
		RowBounds rowBounds = new RowBounds((pageIndex-1)*pageSize,pageSize);
		int total = baseMapper.countByExample(example);
		List<Object> pageList = baseMapper.selectByExampleWithRowbounds(example,rowBounds);
		
		pager.setTotal(total);
		pager.setPageList(pageList);
		return pager;
	}
	
}

