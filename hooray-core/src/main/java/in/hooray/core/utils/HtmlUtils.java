package in.hooray.core.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;

public class HtmlUtils {
	/**
     * 将页面提交的数据字符串进行替换,防止出现页面混乱
     * @param param
     * @return
     * @throws IllegalAccessException
     */
    public static Object replaceStringHtml(Object param) throws IllegalAccessException {
        if (param != null) {

            if (JavaTypeUtils.isBasicType(param)) {

                if (param.getClass().equals(JavaBasicTypeEnum.STRING.getsClass())) {
                    return StringEscapeUtils.escapeHtml4(param.toString());
                }

                return param;
            }

            if (JavaTypeUtils.isArray(param)) {
                Object[] objectArray = (Object[]) param;
                for (int i = 0; i < objectArray.length; i++) {
                    Object object = objectArray[i];
                    if(object==null){
                        continue;
                    }
                    objectArray[i] = replaceStringHtml(object);
                }

                return objectArray;
            }

            if (JavaTypeUtils.isCollection(param)) {
                Collection collection = (Collection) param;

                Collection replaceCollection = new ArrayList();

                Iterator iterator = collection.iterator();
                while (iterator.hasNext()){
                    Object nextObj = iterator.next();
                    if(nextObj==null){
                        continue;
                    }
                    Object o = replaceStringHtml(nextObj);
                    iterator.remove();
                    replaceCollection.add(o);
                }

                collection.addAll(replaceCollection);

                return collection;
            }

            if (JavaTypeUtils.isMap(param)) {
                Map map = (Map) param;
                Set set = map.keySet();
                for (Object obj : set) {
                    Object mapValue = map.get(obj);
                    if(mapValue==null){
                        continue;
                    }
                    Object o = replaceStringHtml(mapValue);
                    map.put(obj, o);
                }

                return map;
            }

            Field[] declaredFields = param.getClass().getDeclaredFields();
            for (Field field : declaredFields) {

                field.setAccessible(true);
                int modifiers = field.getModifiers();
                if(modifiers>=24){
                    continue;
                }

                Object o = field.get(param);
                
                if(o==null){
                    continue;
                }

                Object replaceObj = replaceStringHtml(o);

                if (replaceObj.getClass().equals(JavaBasicTypeEnum.LONG.getsClass())) {
                    field.setLong(param, (Long) (replaceObj));
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.BOOLEAN.getsClass())) {
                    field.setBoolean(param, (Boolean) replaceObj);
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.BYTE.getsClass())) {
                    field.setByte(param, (Byte) replaceObj);
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.CHAR.getsClass())) {
                    field.setChar(param, (Character) replaceObj);
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.DOUBLE.getsClass())) {
                    field.setDouble(param, (Double) replaceObj);
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.FLOAT.getsClass())) {
                    field.setFloat(param, (Float) replaceObj);
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.INTEGER.getsClass())) {
                    field.setInt(param, (Integer) replaceObj);
                } else if (replaceObj.getClass().equals(JavaBasicTypeEnum.SHORT.getsClass())) {
                    field.setShort(param, (Short) replaceObj);
                } else {
                    field.set(param, replaceObj);
                }
            }

            return param;
        }

        return param;
    }
}
