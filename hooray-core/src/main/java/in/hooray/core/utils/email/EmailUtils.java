package in.hooray.core.utils.email;

import jodd.mail.EmailAttachment;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

public class EmailUtils {
	
	/**
	 * 
	 * @param host
	 * @param authUser
	 * @param authPasswd
	 * @return
	 */
	public static SendMailSession createSession(String host, String authUser, String authPasswd) {
		SmtpServer smtpServer = SmtpServer.create(host)
				.authenticateWith(authUser, authPasswd);
		SendMailSession session = smtpServer.createSession();
		return session;		
	}

	/**
	 * 发邮件
	 * @param from		发送人
	 * @param to	   	收件人
	 * @param subject 	主题
	 * @param conent 	内容
	 * @param isHtml 	是否HTML格式
	 * @param attachment 附件
	 */
	public static void sendMail(String from, String to, String subject, String conent, Boolean isHtml, EmailAttachment attachment) {
		
	}
}
