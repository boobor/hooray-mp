package in.hooray.core.common;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class BaseObject extends LinkedHashMap<String, Object>{

	private static final long serialVersionUID = 1L;
	
	public BaseObject() {
        super();
    }

    public BaseObject(Map<String, Object> m) {
        super(m);
    }
    
    public void setId(String id) {
        put("_id", id);
    }

    public String getId() {
        return (String) get("_id");
    }
    
    public String getString(String key) {
        return (String) get(key);
    }

    public long getLong(String key) {
        return (long) get(key);
    }

    public Date getCreatedAt() {
        Date date = new Date();
        date.setTime((long) get("createdAt"));
        return date;
    }

    public Date getUpdatedAt() {
        Date date = new Date();
        date.setTime((long) get("updatedAt"));
        return date;
    }
}
