package in.hooray.ws;

import in.hooray.ws.service.HelloWorldWS;

import java.util.Date;

import org.apache.cxf.frontend.ClientProxyFactoryBean;


public class CxfTest {
	public static void main(String[] args) {
		long s=new Date().getTime();
		ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
		factory.setServiceClass(HelloWorldWS.class);
        factory.setAddress("http://127.0.0.1:8888/services/helloworld");
       // factory.getServiceFactory().setDataBinding(new AegisDatabinding());
        HelloWorldWS client = (HelloWorldWS) factory.create();
        System.out.println(client.sayHi("hooray working"));
        long s1=new Date().getTime();
        System.out.println(s1-s);
	}
}
