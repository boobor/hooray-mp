package in.hooray.ws;

import in.hooray.ws.config.CxfConfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(CxfConfig.class)
public class SecurityWsApplication 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(SecurityWsApplication.class, args);
    }
}
