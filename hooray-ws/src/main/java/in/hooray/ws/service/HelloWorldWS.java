package in.hooray.ws.service;

import javax.jws.WebService;

@WebService
public interface HelloWorldWS {

	String sayHi(String text);
	
}
