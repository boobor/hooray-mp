-- ----------------------------
-- Table structure for wx_plugin
-- ----------------------------
DROP TABLE IF EXISTS `wx_plugin`;
CREATE TABLE `wx_plugin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '插件名称',
  `clazzpath` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;