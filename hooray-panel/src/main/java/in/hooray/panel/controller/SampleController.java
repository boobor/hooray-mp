package in.hooray.panel.controller;

import in.hooray.entity.mp.AccountWechat;
import in.hooray.panel.service.HelloWorldService;
import in.hooray.service.mp.AccountWechatService;
import in.hooray.service.mp.MpAccountService;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;

@Controller
@Description("A controller for handling requests for hello messages")
public class SampleController {

	@Autowired
	private HelloWorldService helloWorldService;
	
	@Autowired
	private MpAccountService accountService;
	
	@Autowired
	private MpAccountService accountService2;
	
	@Autowired
	private AccountWechatService accountWechatService;

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, String> hello() {
		System.out.println(accountService.findAccount("hello"));
		System.out.println(accountService2.findAccount("DDDD"));
		System.out.println(helloWorldService.getHelloMessage());
		System.out.println(accountWechatService.getAccuntWechatByPk(11));
		return Collections.singletonMap("message", this.helloWorldService.getHelloMessage());
	}
	
	@RequestMapping("/wechats")
	@ResponseBody
	public Map<String, Object> doQueryWechats(AccountWechat wechat,
			@RequestParam(required = false, defaultValue = "1") int page,
            @RequestParam(required = false, defaultValue = "10") int rows) {
		Map<String, Object> model = Maps.newHashMap();
		List<AccountWechat> wechats = accountWechatService.selectByAccountWechat(wechat, page, rows);
		model.put("pageinfo", new PageInfo<AccountWechat>(wechats));
		model.put("queryParam", wechat);
		model.put("page", page);
		model.put("rows", rows);
		return model;
	}

	protected static class Message {

		@NotBlank(message = "Message value cannot be empty")
		private String value;

		public String getValue() {
			return this.value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

}
