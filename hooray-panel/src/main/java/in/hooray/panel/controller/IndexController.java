package in.hooray.panel.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


//http://my.oschina.net/u/1013711/blog/207987
// http://blog.javachen.com/2015/01/06/build-app-with-spring-boot-and-gradle
@Controller
public class IndexController {
	
	@RequestMapping(value = "/ibeetl", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest req) {
		ModelAndView view = new ModelAndView();
		view.setViewName("/test_ibeetl");
	    //total 是模板的全局变量，可以直接访问
	    view.addObject("total", 1688);
	    return view;
	}
	
}
