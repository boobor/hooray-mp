package in.hooray.panel.enums;

public enum FileStoragePlatform {

    Qiniu("qiniu"),
    Upyun("upyun");

    private String name;

    FileStoragePlatform(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static FileStoragePlatform get(String name) {
        FileStoragePlatform[] plats = FileStoragePlatform.class.getEnumConstants();
        for (FileStoragePlatform plat : plats) {
            if (name.equals(plat.name)) {
                return plat;
            }
        }
        return null;
    }

}
