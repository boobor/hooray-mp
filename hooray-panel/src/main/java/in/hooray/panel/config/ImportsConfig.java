package in.hooray.panel.config;

import org.springframework.context.annotation.Configuration;


/**
 * 有些配置文件或者类需要在某个工程中引入，但是，因为Spring AutoConfiguration没有扫描到那些文件所在的包路径，
 * 这时候就需要该方式进行引入
 * @author daqing
 *
 */
@Configuration
//@Import({MongodbProperties.class})
public class ImportsConfig {

	
//	@Bean(destroyMethod="destroy")
//	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
//	public MongoDB mongoDb() {
//		MongoDB mDb = new MongoDB();
//		return mDb;
//	}
}
