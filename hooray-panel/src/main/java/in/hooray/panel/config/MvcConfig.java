package in.hooray.panel.config;

import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
// @EnableWebMvc // 启动了该配置，无法直接访问类路径下的静态目录`public`，将会由SpringMVC的DispatchServlet进行分发
@ComponentScan(basePackages={"in.hooray"})
public class MvcConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
	@Qualifier(value="beetlConfig")
	private BeetlGroupUtilConfiguration configuration;
	
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
	
	@Bean(name="beetlConfig")
	public BeetlGroupUtilConfiguration beetlGroupUtilConfiguration() {
		BeetlGroupUtilConfiguration b = new BeetlGroupUtilConfiguration();
		ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
		b.setResourceLoader(resourceLoader);
		b.init();
		return b;
	}
	
	@Bean(name="viewResolver")
	public BeetlSpringViewResolver beetlSpringViewResolver() {
		BeetlSpringViewResolver bsvr = new BeetlSpringViewResolver();
		bsvr.setContentType("text/html;charset=UTF-8");
		bsvr.setSuffix(".btl");
		bsvr.setConfig(configuration);
		bsvr.setGroupTemplate(configuration.getGroupTemplate());
		return bsvr;
	}
	
	@Bean
    public MultipartResolver getMultipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setMaxUploadSize(2 * 1024 * 1024); // 2MB
        return resolver;
    }
	
}
