package in.hooray.panel.config.mybatise;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@Configuration
@ImportResource(locations={"mybatise-spring.xml"})
public class XmlsConfig {

}
