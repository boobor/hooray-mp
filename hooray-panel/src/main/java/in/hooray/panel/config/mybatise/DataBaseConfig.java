//package in.hooray.panel.config.mybatise;
//
//
//import javax.sql.DataSource;
//
//import org.springframework.boot.bind.RelaxedPropertyResolver;
//import org.springframework.context.EnvironmentAware;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import com.alibaba.druid.pool.DruidDataSource;
//
///**
// * 数据库配置
// * http://blog.csdn.net/xiaoyu411502/article/details/48164311
// */
//@Configuration
//@EnableTransactionManagement  
//public class DataBaseConfig implements EnvironmentAware {
//	
//	private RelaxedPropertyResolver propertyResolver; 
//
//	@Bean(name="dataSource", destroyMethod = "close", initMethod="init")  
//	public DataSource dataSource() {
//		DruidDataSource datasource = new DruidDataSource();  
//        datasource.setUrl(propertyResolver.getProperty("url"));  
//        datasource.setDriverClassName(propertyResolver.getProperty("driverClassName"));  
//        datasource.setUsername(propertyResolver.getProperty("username"));  
//        datasource.setPassword(propertyResolver.getProperty("password"));  
//		return datasource;
//	} 
//	
//	/*@Bean(name="readOneDataSource", destroyMethod = "close", initMethod="init")  
//    public DataSource readOneDataSource() {  
//        DruidDataSource datasource = new DruidDataSource();  
//        datasource.setUrl(propertyResolver.getProperty("url"));  
//        datasource.setDriverClassName(propertyResolver.getProperty("driverClassName"));  
//        datasource.setUsername(propertyResolver.getProperty("username"));  
//        datasource.setPassword(propertyResolver.getProperty("password"));  
//        return datasource;  
//    }  
//	
//	@Bean(name="readDataSources")  
//    public List<DataSource> readDataSources(){  
//        List<DataSource> dataSources = Lists.newArrayList(); 
//        dataSources.add(readOneDataSource());  
//        return dataSources;  
//    }  */
//
//	@Override
//	public void setEnvironment(Environment environment) {
//		this.propertyResolver = new RelaxedPropertyResolver(environment, "datasource.druid.");  
//	}
//}
