package in.hooray.panel.aop;

import in.hooray.core.utils.HtmlUtils;

import java.io.IOException;
import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;


@Aspect
@Component
public class ServiceLogAop {

	private static final Logger loggerRecord = LoggerFactory.getLogger(ServiceLogAop.class.getName());
	
	@Around("execution(public * in.hooray.service..*Service.*(..))")
    public Object serviceAOP(ProceedingJoinPoint point) throws IOException {
		Object proceed = null;
		Signature signature = point.getSignature();	// 获取拦截的方法封装的签名对象
		MethodSignature methodSignature = (MethodSignature) signature;
		Method method = methodSignature.getMethod();
		Object target = point.getTarget();
		String className = target.getClass().getName();
		try {
			Object[] args = point.getArgs();
			Object o = HtmlUtils.replaceStringHtml(args);
			
			String jsonString = JSON.toJSONString(args);
			
			if (loggerRecord.isInfoEnabled()) {
                loggerRecord.info("Class:" + className + "  MethodName:" + method.getName() + "  Params:" + jsonString);
            }
			proceed = point.proceed((Object[]) o);
            String ReturnString = JSON.toJSONString(proceed);
            if (loggerRecord.isInfoEnabled()) {
                loggerRecord.info("ReturnMsg:<!--  Class:" + className + "  MethodName:" + method.getName() + "  Content: " + ReturnString + "  -->");
            }
		} catch(Throwable throwable) {
			throw new RuntimeException(throwable);
		}
		return proceed;
	}
}
