package in.hooray.panel.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


@Component
@Aspect
public class DemoServiceMonitor {
	
	@Before("execution(* in.hooray.service..*Service.*(..))")
	public void logAccessServiceStart(JoinPoint joinPoint) {
		System.out.println("Before: " + joinPoint);
	}

	@AfterReturning("execution(* in.hooray.service..*Service.*(..))")
	public void logAccessService(JoinPoint joinPoint){
		System.out.println("Completed: " + joinPoint);
	} 	
}
