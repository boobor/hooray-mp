package in.hooray.panel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication
public class PanelAppRunner extends WebMvcConfigurerAdapter {
	
//	@Autowired
//	private MetricExportProperties export;
//	
//	@Bean
//	@Qualifier("mbeanExporter")
//	public MBeanExporter mBeanExporter() {
//		MBeanExporter exporter = new MBeanExporter();
//		exporter.setAutodetectModeName("AUTODETECT_ALL");
//		return exporter;
//	}
//
//	@Bean
//	@ExportMetricWriter
//	public RedisMetricRepository redisMetricWriter(RedisConnectionFactory connectionFactory) {
//		return new RedisMetricRepository(connectionFactory,
//				this.export.getRedis().getPrefix(), this.export.getRedis().getKey());
//	}
//
//	@Bean
//	@ExportMetricWriter
//	public JmxMetricWriter jmxMetricWriter(@Qualifier("mbeanExporter") MBeanExporter exporter) {
//		return new JmxMetricWriter(exporter);
//	}
	
	
    public static void main( String[] args ) {
    	SpringApplication.run(PanelAppRunner.class, args);
    }
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //Content-Type拦截器
//        registry.addInterceptor(contentTypeInterceptor).excludePathPatterns("/file/callback", "/customer/**");
//        //授权拦截器
//        registry.addInterceptor(authInterceptor).excludePathPatterns("/file/callback", "/admin/**", "/customer/**");
//        //超级权限
//        registry.addInterceptor(adminInterceptor).addPathPatterns("/admin/**");
//        //管理权限
//        registry.addInterceptor(masterInterceptor).addPathPatterns("/master/**");
    }

    @Bean
    public RestTemplate getRest() {
        return new RestTemplate();
    }
    
}
