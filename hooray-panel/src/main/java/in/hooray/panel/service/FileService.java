package in.hooray.panel.service;

import in.hooray.panel.enums.FileStoragePlatform;
import in.hooray.panel.handler.IFileHandler;
import in.hooray.panel.handler.impl.QiniuFileHandler;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileService {
	public static final String FILE_CLASS_NAME = "_File";

	@Autowired
	private QiniuFileHandler qiniuFileHandler;

	public Map<String, Object> getToken(String appId, FileStoragePlatform plat,
			String fileName) throws Exception {
		return getFileHandler(plat).getToken(appId, fileName);
	}

	public Map<String, Object> callback(FileStoragePlatform plat, String body,
			HttpServletRequest request) throws Exception {
		return getFileHandler(plat).callback(body, request);
	}

//	public BaasFile getFile(String appId, String id) throws Exception {
//		BaasObject object = objectService.get(appId, FILE_CLASS_NAME, id);
//		if (object == null) {
//			return null;
//		} else {
//			return new BaasFile(object);
//		}
//	}
//
//	public String saveFile(String appId, BaasFile file) throws Exception {
//		// 禁止设置ACL字段
//		file.remove("acl");
//		file.setKey(getFileKey());
//		return objectService.insert(appId, FILE_CLASS_NAME, file, null, true);
//	}

	private IFileHandler getFileHandler(FileStoragePlatform plat)
			throws Exception {
		IFileHandler handler = null;
		switch (plat) {
		case Qiniu:
			handler = qiniuFileHandler;
			break;
		case Upyun:
			break;
		default:
			break;
		}
		if (handler == null) {
			throw new Exception("101");
		}
		return handler;
	}

	public String getFileKey() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
