package in.hooray.service.mp;

import in.hooray.entity.mp.AccountWechat;
import in.hooray.entity.mp.AccountWechatCriteria;
import in.hooray.service.IBaseService;

import java.util.List;

public interface AccountWechatService extends IBaseService<AccountWechat> {
	
	
	public List<AccountWechat> findAccuntWechats(AccountWechatCriteria criteria);
	
	public AccountWechat getAccuntWechat(AccountWechatCriteria criteria);
	
	public AccountWechat getAccuntWechatByPk(Integer accountId);
	
	/**
     * 根据条件分页查询
     *
     * @param wechat
     * @param page
     * @param rows
     * @return
     */
    List<AccountWechat> selectByAccountWechat(AccountWechat wechat, int page, int rows);

	
}
