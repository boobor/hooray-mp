package in.hooray.service.mp;

import in.hooray.entity.mp.WxPlugin;

import java.util.List;

public interface WxPluginService {

	public List<WxPlugin> getWxPlugins();
	
}
