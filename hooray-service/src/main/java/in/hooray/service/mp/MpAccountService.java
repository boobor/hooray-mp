package in.hooray.service.mp;

import in.hooray.entity.mp.MpAccount;

import java.util.List;


public interface MpAccountService {
	
	
	public List<MpAccount> findAccount(String accountName);
	
}
