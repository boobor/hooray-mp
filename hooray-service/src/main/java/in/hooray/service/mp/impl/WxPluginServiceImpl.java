package in.hooray.service.mp.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.hooray.entity.mapper.WxPluginMapper;
import in.hooray.entity.mp.WxPlugin;
import in.hooray.entity.mp.WxPluginCriteria;
import in.hooray.service.mp.WxPluginService;

/**
 * 项目支持的所有插件
 * 1. 是否支持多个插件同时执行
 * 2. 多个插件同时执行，如何组装返回数据？
 * @author darking
 *
 */
@Service("wxPluginService")
public class WxPluginServiceImpl implements WxPluginService {
	
	@Autowired
	private WxPluginMapper pluginMapper;

	@Override
	public List<WxPlugin> getWxPlugins() {
		WxPluginCriteria criteria = new WxPluginCriteria();
		criteria.createCriteria().andStatusEqualTo(Boolean.TRUE);
		return pluginMapper.selectByExample(criteria);
	}

}
