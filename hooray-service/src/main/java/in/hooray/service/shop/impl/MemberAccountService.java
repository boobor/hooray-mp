package in.hooray.service.shop.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.hooray.entity.mapper.shop.AccountLogMapper;
import in.hooray.entity.mapper.shop.ShopMemberMapper;
import in.hooray.entity.shop.AccountLog;
import in.hooray.service.shop.IMemberAccountService;


@Service("memberAccountService")
public class MemberAccountService implements IMemberAccountService {
	
	@Autowired
	private AccountLogMapper accountLogMapper;
	
	@Autowired
	private ShopMemberMapper shopMemberMapper;
	

	@Override
	public AccountLog addAccountLog(AccountLog accountLog) {
		Integer pk = accountLogMapper.insert(accountLog);
		return accountLogMapper.selectByPrimaryKey(pk);
	}

	@Override
	public void incrMemberPoint(Integer pointValue) {
		shopMemberMapper.selectAll();
	}

}
