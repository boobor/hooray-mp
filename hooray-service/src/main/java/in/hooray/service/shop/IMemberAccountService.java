package in.hooray.service.shop;

import in.hooray.entity.shop.AccountLog;

/**
 * 用户相关的接口
 * @author Administrator
 *
 */
public interface IMemberAccountService {

	public AccountLog addAccountLog(AccountLog accountLog);
	
	public void incrMemberPoint(Integer pointValue);
	
}
